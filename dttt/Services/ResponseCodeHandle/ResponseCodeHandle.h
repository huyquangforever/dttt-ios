//
//  ResponseCodeHandle.h
//  dttt
//
//  Created by Nguyen Huy Quang on 1/10/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef NS_ENUM(NSInteger, MainResponseCode)
{
    MainResponseCodeProcessing              = 98,
    MainResponseCodeWin                     = 99,
    MainResponseCodeNotBegin                = 100,
    MainResponseCodeMissGame                = 101,
    MainResponseCodeAnswered                = 102,
    MainResponseCodeSum                     = 103,
    MainResponseCodeNotRegistered           = 104,
    MainResponseCodeCanNotExtend            = 105,
    MainResponseCodeEnded                   = 106,
    MainResponseCodeUserHasStoped           = 107,
    MainResponseCodeWrongAnswer             = 108,
    MainResponseCodeSystemReceivedAnswer    = 109,
    MainResponseCodeLuckyStarSucess         = 110,
    MainResponseCodeLuckyStarIssue          = 111,
    MainResponseCodeLuckyStarFailure        = 112,
    MainResponseCodeSuccess                 = 0
};
typedef NS_ENUM(NSInteger, PackageResponseCode)
{
    PackageResponseCodeNotBegin                = 100,
    PackageResponseCodeMissGame                = 101,
    PackageResponseCodeAnswered                = 102,
    PackageResponseCodeSum                     = 103,
    PackageResponseCodeNotRegistered           = 104,
    PackageResponseCodeCanNotExtend            = 105,
    PackageResponseCodeEnded                   = 106,
    PackageResponseCodeFinishGame              = 204,
    PackageResponseCodeSuccess                 = 0
};
@interface ResponseCodeHandle : NSObject
+ (NSString *)titleForMainResponseCode:(NSInteger)code;
+ (NSString *)titleForPackageResponseCode:(NSInteger)code;
+ (NSString *)messageForMainResponseCode:(NSInteger)code;
+ (NSString *)messageForPackageResponseCode:(NSInteger)code;
@end
