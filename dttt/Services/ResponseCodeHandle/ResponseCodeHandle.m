//
//  ResponseCodeHandle.m
//  dttt
//
//  Created by Nguyen Huy Quang on 1/10/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import "ResponseCodeHandle.h"

@implementation ResponseCodeHandle
+(NSString *)titleForMainResponseCode:(NSInteger)code
{
    NSString *title = @"";
    switch (code) {
        case MainResponseCodeNotBegin:
            title = @"Chưa tới giờ tham gia đấu trường";
            break;
        case MainResponseCodeSum:
            title = @"Đang tổng hợp kết quả";
            break;
        case MainResponseCodeWin:
            title = @"Chiến thắng";
            break;
        case MainResponseCodeEnded:
            title = @"Kết thúc";
            break;
        case MainResponseCodeSuccess:
            title = @"Thành công";
            break;
        case MainResponseCodeAnswered:
            title = @"Đã trả lời câu hỏi";
            break;
        case MainResponseCodeMissGame:
            title = @"Thuê bao bỏ lỡ đấu trường";
            break;
        case MainResponseCodeProcessing:
            title = @"Đang xử lý yêu cầu";
            break;
        case MainResponseCodeWrongAnswer:
            title = @"Trả lời sai câu hỏi";
            break;
        case MainResponseCodeCanNotExtend:
            title = @"Không thể gia hạn";
            break;
        case MainResponseCodeNotRegistered:
            title = @"Thuê bao chưa đăng kí";
            break;
        case MainResponseCodeUserHasStoped:
            title = @"Thuê bao dừng cuộc chơi";
            break;
        case MainResponseCodeSystemReceivedAnswer:
            title = @"Hệ thống tiếp nhận câu trả lời";
            break;
        case MainResponseCodeLuckyStarSucess:
            title = @"Trả lời đúng câu hỏi với ngôi sao may mắn";
            break;
        case MainResponseCodeLuckyStarIssue:
            title = @"Không đủ tiền để sử dụng ngôi sao may mắn";
            break;
        case MainResponseCodeLuckyStarFailure:
            title = @"Không thể sử dụng ngôi sao may mắn";
            break;
        default:
            break;
    }
    return title;
}

+(NSString *)titleForPackageResponseCode:(NSInteger)code
{
    NSString *title = @"";
    switch (code) {
        case PackageResponseCodeSum:
            title = @"Đang tổng hợp kết quả";
            break;
        case PackageResponseCodeEnded:
            title = @"Đấu trường đã kết thúc";
            break;
        case PackageResponseCodeSuccess:
            title = @"Thành công";
            break;
        case PackageResponseCodeAnswered:
            title = @"Đã trả lời câu hỏi";
            break;
        case PackageResponseCodeMissGame:
            title = @"Thuê bao bỏ lỡ đấu trường";
            break;
        case PackageResponseCodeNotBegin:
            title = @"Chưa tới giờ tham gia đấu trường";
            break;
        case PackageResponseCodeFinishGame:
            title = @"Trả lời đúng toàn bộ câu hỏi";
            break;
        case PackageResponseCodeCanNotExtend:
            title = @"Không thể gia hạn";
            break;
        case PackageResponseCodeNotRegistered:
            title = @"Thuê bao chưa đăng kí";
            break;
        default:
            break;
    }
    return title;
}

+ (NSString *)messageForMainResponseCode:(NSInteger)code
{
    NSString *title = @"";
    switch (code) {
        case MainResponseCodeNotBegin:
            title = @"Chưa tới giờ tham gia đấu trường";
            break;
        case MainResponseCodeSum:
            title = @"Hệ thống đang tổng hợp kết quả.Vui lòng chờ trong giây lát";
            break;
        case MainResponseCodeWin:
            title = @"Chúc mừng bạn đã chiến thắng";
            break;
        case MainResponseCodeEnded:
            title = @"Kết thúc";
            break;
        case MainResponseCodeSuccess:
            title = @"Thành công";
            break;
        case MainResponseCodeAnswered:
            title = @"Đã trả lời câu hỏi";
            break;
        case MainResponseCodeMissGame:
            title = @"Thuê bao bỏ lỡ đấu trường";
            break;
        case MainResponseCodeProcessing:
            title = @"Đang xử lý yêu cầu";
            break;
        case MainResponseCodeWrongAnswer:
            title = @"Trả lời sai câu hỏi";
            break;
        case MainResponseCodeCanNotExtend:
            title = @"Không thể gia hạn";
            break;
        case MainResponseCodeNotRegistered:
            title = @"Thuê bao chưa đăng kí";
            break;
        case MainResponseCodeUserHasStoped:
            title = @"Thuê bao dừng cuộc chơi";
            break;
        case MainResponseCodeSystemReceivedAnswer:
            title = @"Hệ thống tiếp nhận câu trả lời";
            break;
        case MainResponseCodeLuckyStarSucess:
            title = @"Trả lời đúng câu hỏi với ngôi sao may mắn";
            break;
        case MainResponseCodeLuckyStarIssue:
            title = @"Không đủ tiền để sử dụng ngôi sao may mắn";
            break;
        case MainResponseCodeLuckyStarFailure:
            title = @"Không thể sử dụng ngôi sao may mắn";
            break;
        default:
            break;
    }
    return title;
}
+ (NSString *)messageForPackageResponseCode:(NSInteger)code
{
    NSString *title = @"";
    switch (code) {
        case PackageResponseCodeSum:
            title = @"Đang tổng hợp kết quả";
            break;
        case PackageResponseCodeEnded:
            title = @"Đấu trường đã kết thúc";
            break;
        case PackageResponseCodeSuccess:
            title = @"Thành công";
            break;
        case PackageResponseCodeAnswered:
            title = @"Đã trả lời câu hỏi";
            break;
        case PackageResponseCodeMissGame:
            title = @"Thuê bao bỏ lỡ đấu trường";
            break;
        case PackageResponseCodeNotBegin:
            title = @"Chưa tới giờ tham gia đấu trường";
            break;
        case PackageResponseCodeFinishGame:
            title = @"Trả lời đúng toàn bộ câu hỏi";
            break;
        case PackageResponseCodeCanNotExtend:
            title = @"Không thể gia hạn";
            break;
        case PackageResponseCodeNotRegistered:
            title = @"Thuê bao chưa đăng kí";
            break;
        default:
            break;
    }
    return title;
}
@end
