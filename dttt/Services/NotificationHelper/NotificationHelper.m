//
//  NotificationHelper.m
//  dttt
//
//  Created by Nguyen Huy Quang on 1/18/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import "NotificationHelper.h"

@implementation NotificationHelper
+ (instancetype)sharedInstance
{
    static NotificationHelper *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[NotificationHelper alloc] init];
    });
    return instance;
}
- (void)cancelAllNotification
{
    for (UILocalNotification *notify in [UIApplication sharedApplication].scheduledLocalNotifications)
    {
        if (notify.userInfo[@"key"] != nil && [notify.userInfo[@"key"] isEqualToString:@"kQuestionNotifyKey"])
        {
            [[UIApplication sharedApplication] cancelLocalNotification:notify];
        }
    }
}

- (void)makeQuestionNotifyWithMessage:(NSString *)message fireDate:(NSDate *)fireDate
{
    [self cancelAllNotification];
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.userInfo = @{@"key":@"kQuestionNotifyKey"};
    notification.alertBody = message;
    notification.alertTitle = @"Đấu trường tri thức";
    notification.fireDate = fireDate;
    notification.repeatInterval = 0;
    notification.timeZone = [NSTimeZone systemTimeZone];
    
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
}
@end
