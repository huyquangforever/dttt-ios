//
//  NotificationHelper.h
//  dttt
//
//  Created by Nguyen Huy Quang on 1/18/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NotificationHelper : NSObject
+ (instancetype)sharedInstance;


- (void)cancelAllNotification;
- (void)makeQuestionNotifyWithMessage:(NSString *)message fireDate:(NSDate *)fireDate;
@end
