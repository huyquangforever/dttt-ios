//
//  ServerRequestor.m
//  dttt
//
//  Created by Nguyen Huy Quang on 1/5/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import "ServerRequestor.h"
#import "AFNetworking.h"
#import "TokenHelper.h"
#define baseURL @"http://124.158.7.8:9091"
@implementation ServerRequestor
///get user device info
+ (NSString *)deviceID
{
    return [UIDevice currentDevice].identifierForVendor.UUIDString;
}
 + (NSString *)token
{
    return [[TokenHelper sharedInstance] getToken];
}
+(AFHTTPSessionManager *)manager
{
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:baseURL]];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json", nil];
    [manager.requestSerializer setValue:[self deviceID] forHTTPHeaderField:@"deviceId"];
    [manager.requestSerializer setTimeoutInterval:30];
    return manager;
}

///make GET request to server api
///
/// params is nullable
+(void)GETRequestWithAPI:(NSString *)api params:(id)params isRequiredToken:(BOOL)requiredToken completionBlock:(ServerResponseHandler)completion
{
    AFHTTPSessionManager *sessionManager = [self manager];
    if (requiredToken) {
        [sessionManager.requestSerializer setValue:[self token] forHTTPHeaderField:@"token"];
    }
    [sessionManager GET:api parameters:params progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@",responseObject);
        completion?completion(nil, responseObject):0;
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",error);
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
        NSError *responseError = [NSError errorWithDomain:@"DTTT_ERROR_DOMAIN" code:[httpResponse statusCode] userInfo:@{NSLocalizedDescriptionKey: error.localizedDescription?:@"Request failure"}];
        completion?completion(responseError, serializedData):0;
    }];
}

///make POST request to server api
///
/// params is nullable
+ (void)POSTRequestWithAPI:(NSString *)api params:(id)params isRequiredToken:(BOOL)requiredToken completionBlock:(ServerResponseHandler)completion
{
    AFHTTPSessionManager *sessionManager = [self manager];
    if (requiredToken)
    {
        [sessionManager.requestSerializer setValue:[self token] forHTTPHeaderField:@"token"];
    }
    [sessionManager POST:api parameters:params progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@",responseObject);
        completion?completion(nil, responseObject):0;
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",error);
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)task.response;
        NSError *responseError = [NSError errorWithDomain:@"DTTT_ERROR_DOMAIN" code:[httpResponse statusCode] userInfo:@{NSLocalizedDescriptionKey: error.localizedDescription?:@"Request failure"}];
        completion?completion(responseError, serializedData):0;
    }];
}

+(void)PUTRequestWithAPI:(NSString *)api params:(id)params isRequiredToken:(BOOL)requiredToken completionBlock:(ServerResponseHandler)completion
{
    
}

+(void)DELETERequestWithAPI:(NSString *)api params:(id)params isRequiredToken:(BOOL)requiredToken completionBlock:(ServerResponseHandler)completion
{
    
}

+(void)POSTAutoLoginWithAPI:(NSString *)api params:(id)params isRequiredToken:(BOOL)requiredToken completionBlock:(ServerResponseHandler)completion
{
    AFHTTPSessionManager *sessionManager = [self manager];
    [sessionManager.requestSerializer setValue:@"936183989" forHTTPHeaderField:@"msisdn"];
    [sessionManager.requestSerializer setValue:@"123" forHTTPHeaderField:@"deviceId"];
    [sessionManager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [sessionManager POST:api parameters:params progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@",responseObject);
        completion?completion(nil, responseObject):0;
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",error);
        completion?completion(error, nil):0;
    }];
}
@end
