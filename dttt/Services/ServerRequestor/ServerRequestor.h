//
//  ServerRequestor.h
//  dttt
//
//  Created by Nguyen Huy Quang on 1/5/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^ServerResponseHandler)(NSError  *error, id response);
@interface ServerRequestor: NSObject
+ (void)GETRequestWithAPI:(NSString *)api params:(id)params isRequiredToken:(BOOL)requiredToken completionBlock:(ServerResponseHandler)completion;
+ (void)POSTRequestWithAPI:(NSString *)api params:(id)params isRequiredToken:(BOOL)requiredToken completionBlock:(ServerResponseHandler)completion;
+ (void)PUTRequestWithAPI:(NSString *)api params:(id)params isRequiredToken:(BOOL)requiredToken completionBlock:(ServerResponseHandler)completion;
+ (void)DELETERequestWithAPI:(NSString *)api params:(id)params isRequiredToken:(BOOL)requiredToken completionBlock:(ServerResponseHandler)completion;

+ (void)POSTAutoLoginWithAPI:(NSString *)api params:(id)params isRequiredToken:(BOOL)requiredToken completionBlock:(ServerResponseHandler)completion;
@end
