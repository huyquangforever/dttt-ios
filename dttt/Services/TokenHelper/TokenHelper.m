//
//  TokenHelper.m
//  dttt
//
//  Created by Nguyen Huy Quang on 1/9/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import "TokenHelper.h"
#define kTokenKey @"kTokenLoginSaved"
@implementation TokenHelper
{
    NSString *_currentToken;
}
- (instancetype)init
{
    self = [super init];
    if (self)
    {
        [self loadData];
    }
    return self;
}
+ (instancetype)sharedInstance
{
    static TokenHelper *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[TokenHelper alloc] init];
    });
    return instance;
}

- (void)loadData
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kTokenKey] != nil) {
        _currentToken = [[NSUserDefaults standardUserDefaults] stringForKey:kTokenKey];
    }
}

- (void)saveToken:(NSString *)token
{
    if (token != nil)
    {
        _currentToken = token;
        [[NSUserDefaults standardUserDefaults] setObject:token forKey:kTokenKey];
    }
}
- (NSString *)getToken
{
    if (_currentToken == nil || _currentToken.length <= 0) {
        [self loadData];
    }
    return _currentToken;
}

- (BOOL)isLogedIn
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kTokenKey] != nil) {
        return YES;
    }
    return NO;
}
- (void)signOut
{
    _currentToken = nil;
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kTokenKey];
}
@end
