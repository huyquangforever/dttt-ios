//
//  TokenHelper.h
//  dttt
//
//  Created by Nguyen Huy Quang on 1/9/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TokenHelper : NSObject
+ (instancetype) sharedInstance;
- (void)saveToken:(NSString *)token;
- (NSString *)getToken;
- (BOOL)isLogedIn;
- (void)signOut;
@end
