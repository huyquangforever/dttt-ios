//
//  CoundownHelper.m
//  dttt
//
//  Created by Nguyen Huy Quang on 1/17/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import "CoundownHelper.h"

@implementation CoundownHelper
{
    NSTimer *_timer;
    
    void(^TimerBlock)();
}
- (instancetype)init
{
    self = [super init];
    if (self)
    {
        [self startTimer];
    }
    return self;
}
+ (instancetype)sharedInstance
{
    static CoundownHelper *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[CoundownHelper alloc] init];
    });
    return instance;
}
- (void)countdownDidUpdated:(void (^)())completion
{
    TimerBlock = completion;
}
- (void)timerUpdate:(NSTimer *)sender
{
    TimerBlock?TimerBlock():0;
}
- (void)stopTimer
{
    if (_timer != nil)
    {
        [_timer invalidate];
        _timer = nil;
    }
    
}
- (void)startTimer
{
    [self stopTimer];
    _timer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(timerUpdate:) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
}
@end
