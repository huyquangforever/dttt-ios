//
//  CoundownHelper.h
//  dttt
//
//  Created by Nguyen Huy Quang on 1/17/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoundownHelper : NSObject

+ (instancetype)sharedInstance;
- (void)countdownDidUpdated:(void(^)())completion;
- (void)stopTimer;
- (void)startTimer;
@end
