//
//  ServerDataServices.m
//  dttt
//
//  Created by Nguyen Huy Quang on 1/5/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import "ServerDataServices.h"

@implementation ServerDataServices

- (instancetype)init
{
    self = [super init];
    
    return self;
}

///shared an instance of server data services
+ (instancetype)sharedInstance
{
    static ServerDataServices *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[ServerDataServices alloc] init];
    });
    return instance;
}

///get user device info

- (NSString *)osVersion
{
    return [UIDevice currentDevice].systemVersion;
}
///login api with phone number
- (void)loginWithPhoneNumber:(NSString *)phone completionBlock:(ServerResponseHandler)completionBlock
{
    NSDictionary *params = @{
                             @"osType": @"iOS",
                             @"osVersion": [self osVersion]?:@"",
                             @"appLanguage":@"en",
                             @"phoneNumber": phone?:@""
                             };
    [ServerRequestor POSTRequestWithAPI:@"/api/phoneNumber" params:params isRequiredToken:NO completionBlock:completionBlock];
}

- (void)autoLogin:(ServerResponseHandler)completionBlock
{
    NSDictionary *params = @{
                             @"osType": @"iOS",
                             @"osVersion": [self osVersion]?:@"",
                             @"appLanguage":@"en"
                             };
    [ServerRequestor POSTRequestWithAPI:@"/api/autoLogin" params:params isRequiredToken:NO completionBlock:completionBlock];
}

-(void)verifyOTPKey:(NSString *)otpKey phoneNumber:(NSString *)phoneNumber completionBlock:(ServerResponseHandler)completionBlock
{
    NSDictionary *params = @{
                             @"phoneNumber": phoneNumber?:@"",
                             @"otp":otpKey?:@""
                             };
    [ServerRequestor POSTRequestWithAPI:@"/api/otp" params:params isRequiredToken:NO completionBlock:completionBlock];
}

///get answer
-(void)getQuestion:(NSString *)type completionBlock:(ServerResponseHandler)completionBlock
{
    NSDictionary *params = @{
                             @"code":type?:@""
                             };
    [ServerRequestor GETRequestWithAPI:@"/api/question" params:params isRequiredToken:YES completionBlock:completionBlock];
}

/// post answer with answer key and type
- (void)postAnswer:(NSString *)answer questionType:(NSString *)type completionBlock:(ServerResponseHandler)completionBlock
{
    NSDictionary *params = @{
                             @"code":[NSString stringWithFormat:@"%@ %@",type, answer]
                             };
    if (type.length <= 0) {
        params = @{
                   @"code":[NSString stringWithFormat:@"%@",answer]
                   };
    }
    
    
    [ServerRequestor POSTRequestWithAPI:@"/api/answer" params:params isRequiredToken:YES completionBlock:completionBlock];
}

/// get answer result
- (void)getAnswerResult:(ServerResponseHandler)completionBlock
{
    [ServerRequestor GETRequestWithAPI:@"/api/getAnswerResult" params:nil isRequiredToken:YES completionBlock:completionBlock];
}

- (void)getScoreCompletion:(ServerResponseHandler)completionBlock
{
    NSDictionary *params = @{
                             @"code":@"diem a"
                             };
    [ServerRequestor GETRequestWithAPI:@"/api/score" params:params isRequiredToken:YES completionBlock:completionBlock];
}
- (void)checkServicesCompletion:(ServerResponseHandler)completionBlock
{
    NSDictionary *params = @{
                             @"code":@"KTDV"
                             };
    [ServerRequestor GETRequestWithAPI:@"/api/score" params:params isRequiredToken:YES completionBlock:completionBlock];
}
- (void)testAutoLogin:(ServerResponseHandler)completionBlock
{
    NSDictionary *params = @{
                             @"osType": @"android",
                             @"osVersion": @"6.0",
                             @"appLanguage":@"en"
                             };
    [ServerRequestor POSTAutoLoginWithAPI:@"api/autoLogin" params:params isRequiredToken:NO completionBlock:completionBlock];
}
@end
