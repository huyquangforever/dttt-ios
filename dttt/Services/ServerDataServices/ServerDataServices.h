//
//  ServerDataServices.h
//  dttt
//
//  Created by Nguyen Huy Quang on 1/5/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServerRequestor.h"
@interface ServerDataServices : NSObject

+ (instancetype) sharedInstance;
- (void)loginWithPhoneNumber:(NSString *)phone completionBlock:(ServerResponseHandler)completionBlock;
- (void)autoLogin:(ServerResponseHandler)completionBlock;
- (void)verifyOTPKey:(NSString *)otpKey phoneNumber:(NSString *)phoneNumber completionBlock:(ServerResponseHandler)completionBlock;
- (void)getQuestion:(NSString *)type completionBlock:(ServerResponseHandler)completionBlock;
- (void)postAnswer:(NSString *)answer questionType:(NSString *)type completionBlock:(ServerResponseHandler)completionBlock;
- (void)getAnswerResult:(ServerResponseHandler)completionBlock;
- (void)getScoreCompletion:(ServerResponseHandler)completionBlock;
- (void)checkServicesCompletion:(ServerResponseHandler)completionBlock;
- (void)testAutoLogin:(ServerResponseHandler)completionBlock;
@end
