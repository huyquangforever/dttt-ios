//
//  NetworkHandler.m
//  dttt
//
//  Created by Nguyen Huy Quang on 1/6/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import "NetworkHandler.h"
@implementation NetworkHandler

+(instancetype)sharedInstance
{
    static NetworkHandler *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[NetworkHandler alloc] init];
    });
    return instance;
}

/// begin start monitoring network function
- (void)beginMonitorNetwork:(void (^)(AFNetworkReachabilityStatus))completion
{
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        completion?completion(status):0;
    }];
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
}
@end
