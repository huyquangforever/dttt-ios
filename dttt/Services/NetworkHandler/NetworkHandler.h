//
//  NetworkHandler.h
//  dttt
//
//  Created by Nguyen Huy Quang on 1/6/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
@interface NetworkHandler : NSObject
+ (instancetype)sharedInstance;
- (void)beginMonitorNetwork:(void(^)(AFNetworkReachabilityStatus result))completion;
@end
