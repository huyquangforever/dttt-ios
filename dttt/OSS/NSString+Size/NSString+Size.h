//
//  NSString+Size.h
//  dttt
//
//  Created by Nguyen Huy Quang on 1/9/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSString (Size)

- (CGSize)usedSizeForMaxWidth:(CGFloat)width withFont:(UIFont *)font;
- (CGSize)usedSizeForMaxWidth:(CGFloat)width withAttributes:(NSDictionary *)attributes;

@end
