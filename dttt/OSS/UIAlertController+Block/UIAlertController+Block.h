//
//  UIAlertController+Block.h
//  dttt
//
//  Created by Nguyen Huy Quang on 1/10/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertController (Block)

+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message cancelTitle:(NSString *)cancelTitle othersActionTitle:(NSArray<NSString *> *)othersActionTitle cancelAction:(void(^)(void))cancelAction othersAction:(void(^)(NSString *actionTitle))othersAction;
+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message cancelTitle:(NSString *)cancelTitle othersActionTitle:(NSArray<NSString *> *)othersActionTitle cancelAction:(void (^)(void))cancelAction othersAction:(void (^)(NSString *))othersAction fromViewController:(UIViewController *)vc;
@end
