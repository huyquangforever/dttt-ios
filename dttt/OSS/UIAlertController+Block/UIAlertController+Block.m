//
//  UIAlertController+Block.m
//  dttt
//
//  Created by Nguyen Huy Quang on 1/10/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import "UIAlertController+Block.h"
#import "AppDelegate.h"
@implementation UIAlertController (Block)

+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message cancelTitle:(NSString *)cancelTitle othersActionTitle:(NSArray<NSString *> *)othersActionTitle cancelAction:(void (^)(void))cancelAction othersAction:(void (^)(NSString *))othersAction
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:cancelTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        cancelAction?cancelAction():0;
    }];
    [alertController addAction:cancel];
    if (othersActionTitle != nil)
    {
        for (NSString *actionTitle in othersActionTitle)
        {
            UIAlertAction *action = [UIAlertAction actionWithTitle:actionTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                othersAction?othersAction(action.title):0;
            }];
            [alertController addAction:action];
        }
    }
    
    AppDelegate *appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appdelegate.window.rootViewController presentViewController:alertController animated:YES completion:^{
        
    }];
}
+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message cancelTitle:(NSString *)cancelTitle othersActionTitle:(NSArray<NSString *> *)othersActionTitle cancelAction:(void (^)(void))cancelAction othersAction:(void (^)(NSString *))othersAction fromViewController:(UIViewController *)vc
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:cancelTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        cancelAction?cancelAction():0;
    }];
    [alertController addAction:cancel];
    if (othersActionTitle != nil)
    {
        for (NSString *actionTitle in othersActionTitle)
        {
            UIAlertAction *action = [UIAlertAction actionWithTitle:actionTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                othersAction?othersAction(action.title):0;
            }];
            [alertController addAction:action];
        }
    }
    [vc presentViewController:alertController animated:YES completion:^{
        
    }];
}
@end
