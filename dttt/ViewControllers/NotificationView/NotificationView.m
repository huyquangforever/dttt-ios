//
//  NotificationView.m
//  dttt
//
//  Created by Nguyen Huy Quang on 1/6/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import "NotificationView.h"
#import "NSString+Size.h"
#import "AppDelegate.h"
@implementation NotificationView
{
    NSString *_message;
    BOOL isSwiped;
}
- (void)awakeFromNib
{
    [super awakeFromNib];
    self.viewContainer.layer.shadowColor = [UIColor blackColor].CGColor;
    self.viewContainer.layer.shadowRadius = 8.0;
    self.viewContainer.layer.shadowOpacity = 0.6;
    self.viewContainer.layer.shadowOffset = CGSizeMake(0, 5);
}
+ (void)showMessage:(NSString *)message
{
    AppDelegate *appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    for (UIView *subview in appdelegate.window.rootViewController.view.subviews)
    {
        if ([subview isKindOfClass:[NotificationView class]]) {
            [(NotificationView *)subview hide];
            break;
        }
    }
    NotificationView *notificationView = (NotificationView *)[[[NSBundle mainBundle] loadNibNamed:@"NotificationView" owner:nil options:nil] firstObject];
    [notificationView setMessage:message];
    [notificationView addSwipeGestures];
    [notificationView setTimeHidden];
    
    notificationView.viewContainer.transform = CGAffineTransformMakeTranslation(0, -notificationView.frame.size.height - 20);
    notificationView.btnClose.transform = CGAffineTransformMakeTranslation(0, -notificationView.frame.size.height - 20);
    [appdelegate.window.rootViewController.view addSubview:notificationView];
    [UIView animateWithDuration:0.5 animations:^{
        notificationView.viewContainer.transform = CGAffineTransformIdentity;
        notificationView.btnClose.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)setMessage:(NSString *)message
{
    _message = message;
    CGSize messageSize = [_message usedSizeForMaxWidth:([UIScreen mainScreen].bounds.size.width - 32) withFont:[UIFont systemFontOfSize:15]];
    NSInteger height = messageSize.height + 35;
    self.frame = CGRectMake(0, 20, [UIScreen mainScreen].bounds.size.width, height);
    _lblTitle.text = _message;
}
- (void)addSwipeGestures
{
    UISwipeGestureRecognizer *swipeGestures = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeAction:)];
    swipeGestures.direction = UISwipeGestureRecognizerDirectionUp;
    swipeGestures.numberOfTouchesRequired = 1;
    [self addGestureRecognizer:swipeGestures];
}
- (void)addPangestures
{
    
}
- (IBAction)close:(id)sender {
    isSwiped = YES;
    [UIView animateWithDuration:0.5 animations:^{
        self.viewContainer.transform = CGAffineTransformMakeTranslation(0, -self.frame.size.height - 20);
        self.btnClose.transform = CGAffineTransformMakeTranslation(0, -self.frame.size.height - 20);
    } completion:^(BOOL finished)
     {
         [self removeFromSuperview];
     }];
}
- (void)setTimeHidden
{
    [self performSelector:@selector(hide) withObject:nil afterDelay:10.0];
}
- (void)hide
{
    if (isSwiped) {
        return;
    }
    isSwiped = YES;
    [UIView animateWithDuration:0.5 animations:^{
        self.viewContainer.transform = CGAffineTransformMakeTranslation(0, -self.frame.size.height - 20);
        self.btnClose.transform = CGAffineTransformMakeTranslation(0, -self.frame.size.height - 20);
    } completion:^(BOOL finished)
    {
        [self removeFromSuperview];
    }];
}
#pragma mark - swipe action
- (void)swipeAction:(UISwipeGestureRecognizer *)sender
{
    if (isSwiped)
    {
        return;
    }
    isSwiped = YES;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.5 animations:^{
            self.viewContainer.transform = CGAffineTransformMakeTranslation(0, -self.frame.size.height - 20);
            self.btnClose.transform = CGAffineTransformMakeTranslation(0, -self.frame.size.height - 20);
        } completion:^(BOOL finished) {
            [self removeFromSuperview];
        }];
    });
}
@end
