//
//  WebviewViewController.h
//  dttt
//
//  Created by Nguyen Huy Quang on 1/10/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebviewViewController : UIViewController

- (void)setUrlString:(NSString *)urlString;
@end
