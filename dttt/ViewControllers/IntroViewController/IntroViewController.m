//
//  IntroViewController.m
//  dttt
//
//  Created by Nguyen Huy Quang on 1/10/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import "IntroViewController.h"
#import "ViewMoreCell.h"
#import "IntroCell.h"
#import "RegisterViewController.h"
#import <SafariServices/SafariServices.h>
#import "WebViewCell.h"
@interface IntroViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
{
    NSString *_textToDisplay;
}

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@end

@implementation IntroViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initNavigationBarItem];
    [self registerCustomCell];
    [self settingTextToDisplay];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - init navigation bar item
- (void)initNavigationBarItem
{
    
    UIBarButtonItem *menuItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_menu"] style:UIBarButtonItemStyleDone target:self action:@selector(menuButtonDidTouch:)];
    self.navigationItem.leftBarButtonItem = menuItem;
}

#pragma mark - bar button item action
- (void)menuButtonDidTouch:(UIBarButtonItem *)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        
    }];
}
#pragma mark - initial text content
- (void)settingTextToDisplay
{
    if (_introType == IntroTypeIntro)
    {
        self.navigationItem.title = @"Giới Thiệu";
        _textToDisplay = @"\"Đấu trường tri thức\" là một dịch vụ đấu trí trực tiếp giữa nhiều người chơi, người chơi có thể tham gia các cuộc thi tri thức thuộc các lĩnh vực khác nhau của đời sống như: Thể thao, Giải trí, Kinh tế, Xã hội, Khoa học, Nghệ thuật....\n";
    }
    else if (_introType == IntroTypePrize)
    {
        self.navigationItem.title = @"Giải Thưởng";
        _textToDisplay = @"Tổng giá trị giải thưởng chương trình khuyến mại Đấu trường tri thức: 50.000.000đ (Bằng chữ: Năm mươi triệu đồng chẵn.)\n";
    }
    
    [_collectionView reloadData];
}
#pragma mark - show webview action
- (void)showWebView
{
    SFSafariViewController *safariVC = [[SFSafariViewController alloc] initWithURL:[NSURL URLWithString:@"http://dautruongtrithuc.mobifone.com.vn"]];
    [self presentViewController:safariVC animated:YES completion:^{
        
    }];

}
#pragma mark - collection view datasource
- (void)registerCustomCell
{
    [_collectionView registerNib:[UINib nibWithNibName:@"ViewMoreCell" bundle:nil] forCellWithReuseIdentifier:@"ViewMoreCell"];
    [_collectionView registerNib:[UINib nibWithNibName:@"IntroCell" bundle:nil] forCellWithReuseIdentifier:@"IntroCell"];
    [_collectionView registerNib:[UINib nibWithNibName:@"WebViewCell" bundle:nil] forCellWithReuseIdentifier:@"WebViewCell"];
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
//    if (indexPath.row == 0)
//    {
//        IntroCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"IntroCell" forIndexPath:indexPath];
//        [cell setText:_textToDisplay];
//        return cell;
//    }
//    else
//    {
//        ViewMoreCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ViewMoreCell" forIndexPath:indexPath];
//        [cell setLink:@"http://dautruongtrithuc.mobifone.com.vn"];
//        [cell.btnLinnk addTarget:self action:@selector(showWebView) forControlEvents:UIControlEventTouchUpInside];
//        return cell;
//    }
    WebViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WebViewCell" forIndexPath:indexPath];
    [cell makeIntro:_introType];
    return cell;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 1;
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

#pragma mark - collection view delegate
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsZero;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return self.view.bounds.size;
//    if (indexPath.row == 0)
//    {
//        return [IntroCell cellSizeWithText:_textToDisplay];
//    }
//    else
//    {
//        return CGSizeMake(self.view.frame.size.width, 80);
//    }
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    return CGSizeZero;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeZero;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}


@end
