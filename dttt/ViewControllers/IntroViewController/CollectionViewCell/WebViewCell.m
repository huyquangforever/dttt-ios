//
//  WebViewCell.m
//  dttt
//
//  Created by Nguyen Huy Quang on 1/25/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import "WebViewCell.h"
@interface WebViewCell() <UIWebViewDelegate>

@end
@implementation WebViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _webview.delegate = self;
}
- (void)dealloc
{
    [SVProgressHUD dismiss];
}
- (void)makeIntro:(NSInteger)introType
{
    if (introType == 0) {
        [_webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://dautruongtrithuc.mobifone.com.vn/DauTruong_WAP/Admin/about.xhtml"]]];
    }
    else
    {
        [_webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://dautruongtrithuc.mobifone.com.vn/DauTruong_WAP/Admin/rules.xhtml"]]];
    }
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *html = [webView stringByEvaluatingJavaScriptFromString:@"var elements = document.getElementsByClassName(\"sidebar-toggle\");var i = 0;while(elements.length > 0){elements[i].style.display = 'none'; i += 1;};var elements2 = document.getElementsByClassName(\"user-image\");var j = 0;while(elements2.length > 0){elements2[j].style.display = 'none'; j += 1;};"];
    [SVProgressHUD dismiss];
}
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [SVProgressHUD show];
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if ([request.URL.absoluteString isEqualToString:@"http://dautruongtrithuc.mobifone.com.vn/DauTruong_WAP/Admin/about.xhtml"] || [request.URL.absoluteString isEqualToString:@"http://dautruongtrithuc.mobifone.com.vn/DauTruong_WAP/Admin/rules.xhtml"])
    {
        return YES;
    }
    return NO;
}
@end
