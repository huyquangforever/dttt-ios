//
//  IntroCell.m
//  dttt
//
//  Created by Nguyen Huy Quang on 1/10/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import "IntroCell.h"
#import "NSString+Size.h"
@implementation IntroCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setText:(NSString *)text
{
//    _textView.font = [UIFont systemFontOfSize:16];
//    _textView.text =  text;
    [_textView setAttributedText:[[NSAttributedString alloc] initWithString:text attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:16]}]];
}

+ (CGSize)cellSizeWithText:(NSString *)text
{
    NSInteger maxWidth = [UIScreen mainScreen].bounds.size.width - 16;
    CGSize questionSize = [text usedSizeForMaxWidth:maxWidth withFont:[UIFont systemFontOfSize:16]];
    NSInteger height = questionSize.height + 16;
    return CGSizeMake([UIScreen mainScreen].bounds.size.width, height);
}
@end
