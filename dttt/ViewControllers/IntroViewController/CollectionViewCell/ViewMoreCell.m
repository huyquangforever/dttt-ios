//
//  ViewMoreCell.m
//  dttt
//
//  Created by Nguyen Huy Quang on 1/10/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import "ViewMoreCell.h"

@implementation ViewMoreCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setLink:(NSString *)link
{
    [_btnLinnk setTitle:link forState:UIControlStateNormal];
    if ([UIScreen mainScreen].bounds.size.width < 375) {
        _btnLinnk.titleLabel.font = [UIFont systemFontOfSize:11];
    }
}
@end
