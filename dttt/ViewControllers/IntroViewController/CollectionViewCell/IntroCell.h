//
//  IntroCell.h
//  dttt
//
//  Created by Nguyen Huy Quang on 1/10/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntroCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UITextView *textView;
- (void)setText:(NSString *)text;

+ (CGSize)cellSizeWithText:(NSString *)text;
@end
