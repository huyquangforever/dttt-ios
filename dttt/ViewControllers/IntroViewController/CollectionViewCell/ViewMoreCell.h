//
//  ViewMoreCell.h
//  dttt
//
//  Created by Nguyen Huy Quang on 1/10/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewMoreCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *viewContainer;
@property (weak, nonatomic) IBOutlet UIButton *btnLinnk;
- (void)setLink:(NSString *)link;
@end
