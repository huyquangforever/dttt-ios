//
//  WebViewCell.h
//  dttt
//
//  Created by Nguyen Huy Quang on 1/25/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIWebView *webview;
- (void)makeIntro:(NSInteger)introType;
@end
