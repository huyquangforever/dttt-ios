//
//  IntroViewController.h
//  dttt
//
//  Created by Nguyen Huy Quang on 1/10/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger, IntroType)
{
    IntroTypeIntro,
    IntroTypePrize,
    IntroTypeWinner
};
@interface IntroViewController : UIViewController
@property (assign, nonatomic) IntroType introType;
@end
