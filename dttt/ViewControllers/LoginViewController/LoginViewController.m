//
//  LoginViewController.m
//  dttt
//
//  Created by Nguyen Huy Quang on 1/6/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import "LoginViewController.h"
#import "LoginCell.h"
#import "ServerDataServices.h"
#import "HomeViewController.h"
#import "MenuViewController.h"
#import "AppDelegate.h"
#import "TokenHelper.h"
@interface LoginViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
{
    NSString *_phoneNumber;
}
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self registerCustomCell];
    [self requestToken];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)requestToken
{
    if (self.needRequestToken)
    {
        [[ServerDataServices sharedInstance] autoLogin:^(NSError *error, id response) {
            
        }];
    }
}
#pragma mark - validate phone number
- (NSString *)validatePhoneNumber:(NSString *)phoneNumber
{
    if ([phoneNumber characterAtIndex:0] != '0')
    {
        return phoneNumber;
    }
    else
    {
        NSString *newPhoneNumber = [phoneNumber substringFromIndex:1];
        return [self validatePhoneNumber:newPhoneNumber];
    }
}
#pragma mark - login did success
- (void)loginDidSuccess
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
//    HomeViewController *homeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
//    MenuViewController *menuVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
//    UINavigationController *naviHome = [[UINavigationController alloc] initWithRootViewController:homeVC];
//    naviHome.navigationBar.titleTextAttributes = @{
//                                                   NSForegroundColorAttributeName: [UIColor whiteColor],
//                                                   NSFontAttributeName: [UIFont systemFontOfSize:18]
//                                                   };
//    naviHome.navigationBar.tintColor = [UIColor whiteColor];
//    naviHome.navigationBar.translucent = NO;
//    naviHome.navigationBar.barTintColor = kMainBlueColor;
//    naviHome.view.backgroundColor = kMainBlueColor;
//    MFSideMenuContainerViewController *containerVC = [MFSideMenuContainerViewController containerWithCenterViewController:naviHome leftMenuViewController:menuVC rightMenuViewController:nil];
//    containerVC.panMode = MFSideMenuPanModeNone;
//    AppDelegate *appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
//    appdelegate.window.rootViewController = containerVC;
}

#pragma mark - cell button action
- (void)backButtonDidTouch:(UIButton *)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}
- (void)sendButtonDidTouch:(UIButton *)sender
{
    LoginCell *cell = (LoginCell *)[_collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    if (cell.isVerifyOTP)
    {
        NSString *otpCode = cell.txtInput.text;
        if (otpCode.length <= 0)
        {
            return;
        }
        [SVProgressHUD show];
        [[ServerDataServices sharedInstance] verifyOTPKey:otpCode phoneNumber:_phoneNumber completionBlock:^(NSError *error, id response)
        {
            [SVProgressHUD dismiss];
            [cell.txtInput resignFirstResponder];
            if (error)
            {
                if (error.code == 401)
                {
                    [UIAlertController showAlertWithTitle:@"Mã OTP không đúng" message:@"Xin vui lòng thử lại" cancelTitle:@"Đóng" othersActionTitle:nil cancelAction:^{
                        
                    } othersAction:^(NSString *actionTitle) {
                        
                    } fromViewController:self];
                }
            }
            else
            {
                NSString *token = response[@"token"];
                [[TokenHelper sharedInstance] saveToken:token];
                [self loginDidSuccess];
            }
            
        }];
    }
    else
    {
        NSString *phoneNumber = cell.txtInput.text;
        if (phoneNumber.length <= 0) {
            return;
        }
//        _phoneNumber = phoneNumber;
        _phoneNumber = [self validatePhoneNumber:phoneNumber];
        [[ServerDataServices sharedInstance] loginWithPhoneNumber:_phoneNumber completionBlock:nil];
        [cell makeViewVerifyOTP];
    }
}
#pragma mark - collection view datasource
- (void)registerCustomCell
{
    [_collectionView registerNib:[UINib nibWithNibName:@"LoginCell" bundle:nil] forCellWithReuseIdentifier:@"LoginCell"];
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    LoginCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LoginCell" forIndexPath:indexPath];
    [cell.btnBack addTarget:self action:@selector(backButtonDidTouch:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnDone addTarget:self action:@selector(sendButtonDidTouch:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 1;
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

#pragma mark - collection view delegate
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsZero;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [LoginCell cellSize];
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    return CGSizeZero;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeZero;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

@end
