//
//  LoginCell.m
//  dttt
//
//  Created by Nguyen Huy Quang on 1/6/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import "LoginCell.h"
@interface LoginCell() <CAAnimationDelegate>

@end
@implementation LoginCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self registerKeyboardListner];
}
/// return size of cell's instance
+ (CGSize)cellSize
{
    return [UIScreen mainScreen].bounds.size;
}

#pragma makr - keyboard handler
- (void)registerKeyboardListner
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardDidShow:(NSNotification *)noti
{
    CGRect keyboardFrame = [noti.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    NSInteger contentHeigt = screenSize.height - keyboardFrame.size.height;
    CGRect viewContentFrame = _viewContent.frame;
    viewContentFrame.origin.y = (contentHeigt - viewContentFrame.size.height)/2;
    [UIView animateWithDuration:0.3 animations:^{
        _viewContent.frame = viewContentFrame;
    } completion:^(BOOL finished) {
        
    }];
}
- (void)keyboardDidHide:(NSNotification *)noti
{
//    [UIView animateWithDuration:0.3 animations:^{
//        _viewContent.center = self.center;
//    } completion:^(BOOL finished) {
//        
//    }];
    
}

/// make current view change to verify OTP code
- (void)makeViewVerifyOTP
{
    _isVerifyOTP = YES;
    [_txtInput resignFirstResponder];
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    transition.delegate = self;
    [_viewContent.layer addAnimation:transition forKey:nil];
}

#pragma mark - animation delegate
-(void)animationDidStart:(CAAnimation *)anim
{
    _txtInput.text = @"";
    _txtInput.placeholder = @"Nhập mã xác thực";
    _txtInput.keyboardType = UIKeyboardTypeDefault;
    [_btnDone setTitle:@"Xác thực" forState:UIControlStateNormal];
}
- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    
}
@end
