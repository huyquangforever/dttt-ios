//
//  PackageGamePlayViewController.m
//  dttt
//
//  Created by Nguyen Huy Quang on 1/11/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import "PackageGamePlayViewController.h"
#import "QuestionCell.h"
#import "AnswerCell.h"
#import "ServerDataServices.h"
#import "PackageQuestionEntity.h"
#import "LoginViewController.h"
#import "NotificationView.h"
#import "RegisterViewController.h"
#import <SafariServices/SafariServices.h>
@interface PackageGamePlayViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
{
    NSString *currentPackageCode;
    
    NSString *question;
    
    NSInteger currentAnswer;
    PackageQuestionEntity *questionEntity;
}
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@end

@implementation PackageGamePlayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
    [self registerCustomCell];
    [self loadQuestion];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)back:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - load question
- (void)loadQuestion
{
    [SVProgressHUD show];
    NSString *code = @"CH";
    switch (_gamePlayType) {
        case GamePlayTypeMain:
            code = @"CH";
            currentPackageCode = @"";
            break;
        case GamePlayTypeSport:
            code = @"CH T";
            currentPackageCode = @"T";
            self.collectionView.backgroundColor = [UIColor colorWithRed:52.0/255.0 green:73.0/255.0 blue:94.0/255.0 alpha:1.0];
            break;
        case GamePlayTypeMusic:
            code = @"CH A";
            currentPackageCode = @"A";
            self.collectionView.backgroundColor = [UIColor colorWithRed:22.0/255.0 green:160.0/255.0 blue:133.0/255.0 alpha:1.0];
            break;
        case GamePlayTypeFun:
            code = @"CH D";
            currentPackageCode = @"D";
            self.collectionView.backgroundColor = [UIColor colorWithRed:243.0/255.0 green:156.0/255.0 blue:18.0/255.0 alpha:1.0];
            break;
        case GamePlayTypeHistory:
            code = @"CH L";
            currentPackageCode = @"L";
            self.collectionView.backgroundColor = [UIColor colorWithRed:41.0/255.0 green:128.0/255.0 blue:185.0/255.0 alpha:1.0];
            break;
        default:
            break;
    }
    [[ServerDataServices sharedInstance] getQuestion:code completionBlock:^(NSError *error, id response) {
        [SVProgressHUD dismiss];
        if (error)
        {
            if (error.code == 401)
            {
                [UIAlertController showAlertWithTitle:@"Đăng nhập đã hết hạn" message:@"Bạn cần phải đăng nhập lại để tiếp tục." cancelTitle:@"Đăng nhập" othersActionTitle:nil cancelAction:^{
                    [[TokenHelper sharedInstance] signOut];
                    LoginViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                    [self presentViewController:loginVC animated:YES completion:^{
                        
                    }];
                } othersAction:nil];
            }
            else
            {
                NSString *message = response[@"message"];
                NSInteger code = [response[@"code"] integerValue];
                if (code == 104)
                {
//                    [UIAlertController showAlertWithTitle:@"Chưa đăng kí gói" message:message cancelTitle:@"Đóng" othersActionTitle:nil cancelAction:^{
//                        [self.navigationController popViewControllerAnimated:YES];
//                    } othersAction:^(NSString *action) {
//                        
//                    } fromViewController:self];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        NSString *url = [NSString stringWithFormat:@"http://dautruongtrithuc.mobifone.com.vn/WapGW/subscribe?pkgcode=%@&pkgid=1&price=2000&info=DTTT-2000&id=1&agentid=3",currentPackageCode];
                        SFSafariViewController *safariVC = [[SFSafariViewController alloc] initWithURL:[NSURL URLWithString:url]];
                        [self presentViewController:safariVC animated:YES completion:nil];
                    });
                    
//                    [UIAlertController showAlertWithTitle:@"Chưa đăng kí gói" message:message cancelTitle:@"Đóng" othersActionTitle:@[@"Đăng kí"] cancelAction:^{
//                        
//                    } othersAction:^(NSString *action) {
//                    SFSafariViewController *safariVC = [[SFSafariViewController alloc] initWithURL:[NSURL URLWithString:@"http://dautruongtrithuc.mobifone.com.vn/NewRegister.html"]];
//                    [self presentViewController:safariVC animated:YES completion:nil];
//                    RegisterViewController *registerVC = [[RegisterViewController alloc] initWithURL:[NSURL URLWithString:@"http://dautruongtrithuc.mobifone.com.vn/NewRegister.html"]];
//                    registerVC.needUpdateStatusBar = YES;
//                    [self presentViewController:registerVC animated:YES completion:^{
//                            
//                    }];
//                    } fromViewController:self];
                    return;
                }
                if (_gamePlayType == GamePlayTypeMain)
                {
                    [UIAlertController showAlertWithTitle:[ResponseCodeHandle titleForMainResponseCode:code] message:message cancelTitle:@"Đóng" othersActionTitle:nil cancelAction:^{
                        [self.navigationController popViewControllerAnimated:YES];
                    } othersAction:nil];
                }
                else
                {
                    [UIAlertController showAlertWithTitle:[ResponseCodeHandle titleForPackageResponseCode:code] message:message cancelTitle:@"Đóng" othersActionTitle:nil cancelAction:^{
                        [self.navigationController popViewControllerAnimated:YES];
                    } othersAction:nil];
                }
            }
        }
        else
        {
            NSDictionary *dict = response;
            [self setQuestionData:dict];
        }
    }];
}
- (void)sendAnswer
{
    [SVProgressHUD show];
    [[ServerDataServices sharedInstance] postAnswer:[NSString stringWithFormat:@"%d",currentAnswer] questionType:currentPackageCode completionBlock:^(NSError *error, id response) {
        
        if (error)
        {
            [SVProgressHUD dismiss];
            if (error.code == 401)
            {
                [UIAlertController showAlertWithTitle:@"Đăng nhập đã hết hạn" message:@"Bạn cần phải đăng nhập lại để tiếp tục." cancelTitle:@"Đăng nhập" othersActionTitle:nil cancelAction:^{
                    [[TokenHelper sharedInstance] signOut];
                    LoginViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                    [self presentViewController:loginVC animated:YES completion:^{
                        
                    }];
                } othersAction:nil];
            }
            else
            {
                NSString *message = response[@"message"];
                NSInteger code = [response[@"code"] integerValue];
                if (code == 104)
                {
//                    [UIAlertController showAlertWithTitle:@"Chưa đăng kí gói" message:message cancelTitle:@"Đóng" othersActionTitle:nil cancelAction:^{
//                        [self.navigationController popViewControllerAnimated:YES];
//                    } othersAction:^(NSString *action) {
//                        
//                    } fromViewController:self];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        NSString *url = [NSString stringWithFormat:@"http://dautruongtrithuc.mobifone.com.vn/WapGW/subscribe?pkgcode=%@&pkgid=1&price=2000&info=DTTT-2000&id=1&agentid=3",currentPackageCode];
                        SFSafariViewController *safariVC = [[SFSafariViewController alloc] initWithURL:[NSURL URLWithString:url]];
                        [self presentViewController:safariVC animated:YES completion:nil];
                    });
                    //                    RegisterViewController *registerVC = [[RegisterViewController alloc] initWithURL:[NSURL URLWithString:@"http://dautruongtrithuc.mobifone.com.vn/NewRegister.html"]];
//                    registerVC.needUpdateStatusBar = YES;
//                    [self presentViewController:registerVC animated:YES completion:^{
//                            
//                    }];
                    return;
                }
                [NotificationView showMessage:message];

            }

            [self loadQuestion];
        }
        else
        {
            NSDictionary *dict = response;
            NSInteger isSuccess = dict[@"isSuccess"]?[dict[@"isSuccess"] integerValue]:0;
            NSString *packageScore = dict[@"packageScore"]?:@"";
            NSString *endScore = dict[@"endScore"]?:@"";
            if (isSuccess == 1)
            {
                if (endScore.length > 0)
                {
                    NSString *message = [NSString stringWithFormat:@"Bạn đã trả lời chính xác câu hỏi số 5! Tổng số điểm của bạn hiện tại là %@",endScore];
                    [NotificationView showMessage:message];
                }
                else
                {
                    NSString *message = [NSString stringWithFormat:@"Bạn đã trả lời chính xác! số điểm của bạn hiện tại là %@",packageScore];
                    [NotificationView showMessage:message];
                }
            }
            else
            {
                [SVProgressHUD showErrorWithStatus:@"Bạn đã trả lời sai"];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                });
            }
            [self loadQuestion];
        }
    }];
}
#pragma mark - setquestion data
- (void)setQuestionData:(NSDictionary *)dict
{
    questionEntity = [[PackageQuestionEntity alloc] init];
    [questionEntity parseObject:dict];
    self.lblTitle.text = [NSString stringWithFormat:@"Câu hỏi %d",questionEntity.QuestionNum];
    [self.collectionView reloadData];
}
#pragma mark - collection view datasource
- (void)registerCustomCell
{
    [_collectionView registerNib:[UINib nibWithNibName:@"AnswerCell" bundle:nil] forCellWithReuseIdentifier:@"AnswerCell"];
    [_collectionView registerNib:[UINib nibWithNibName:@"QuestionCell" bundle:nil] forCellWithReuseIdentifier:@"QuestionCell"];
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        QuestionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"QuestionCell" forIndexPath:indexPath];
        [cell setQuestion:questionEntity.question];
        return cell;
    }
    else
    {
        AnswerCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"AnswerCell" forIndexPath:indexPath];
        cell.lblAnswer.text = [NSString stringWithFormat:@"Đáp án %d", indexPath.row + 1];
        return cell;
    }
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (section == 0)
    {
        return questionEntity.question?1:0;
    }
    else
    {
        return questionEntity.numAnswer;
    }
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 2;
}

#pragma mark - collection view delegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1)
    {
        currentAnswer = indexPath.row + 1;
        [UIAlertController showAlertWithTitle:@"" message:[NSString stringWithFormat:@"Bạn có chắc chắn câu trả lời của bạn là đáp án %d?",currentAnswer] cancelTitle:@"Chọn lại" othersActionTitle:@[@"Đúng"] cancelAction:^{
            
        } othersAction:^(NSString *action) {
            [self sendAnswer];
        } fromViewController:self];

    }
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if (section == 0)
    {
        return UIEdgeInsetsMake(0, 15, 60, 15);
    }
    else
    {
        return UIEdgeInsetsMake(0, 15, 15, 15);
    }
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return [QuestionCell cellSizeWithQuestion:questionEntity.question];
    }
    else
    {
        return [AnswerCell cellSize];
    }
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    return CGSizeZero;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return CGSizeMake(collectionView.frame.size.width, 90);
    }
    return CGSizeZero;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 15;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 15;
}


@end
