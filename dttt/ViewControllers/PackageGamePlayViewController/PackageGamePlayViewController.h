//
//  PackageGamePlayViewController.h
//  dttt
//
//  Created by Nguyen Huy Quang on 1/11/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GamePlayViewController.h"
@interface PackageGamePlayViewController : UIViewController
@property (assign, nonatomic) GamePlayType gamePlayType;
@end
