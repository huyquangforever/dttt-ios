//
//  StartPageCell.m
//  dttt
//
//  Created by Nguyen Huy Quang on 1/6/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import "StartPageCell.h"

@implementation StartPageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

/// return size of cell's instance
+ (CGSize)cellSize
{
    return [UIScreen mainScreen].bounds.size;
}
@end
