//
//  StartPageViewController.m
//  dttt
//
//  Created by Nguyen Huy Quang on 1/6/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import "StartPageViewController.h"
#import "StartPageCell.h"
#import "HomeViewController.h"
#import "MenuViewController.h"
#import "MFSideMenu.h"
#import "AppDelegate.h"
#import "LoginViewController.h"
#import "NetworkHandler.h"
#import "ServerDataServices.h"
#import "RegisterViewController.h"
#import "TokenHelper.h"

@interface StartPageViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@end

@implementation StartPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self registerCustomCell];
    if ([[TokenHelper sharedInstance] isLogedIn])
    {
        [self loginDidSuccess];
    }
    else
    {
        [self autoLogin];
//        [[NetworkHandler sharedInstance] beginMonitorNetwork:^(AFNetworkReachabilityStatus result) {
//            if (result == AFNetworkReachabilityStatusReachableViaWWAN)
//            {
//                [self autoLogin];
//            }
//            else if (result == AFNetworkReachabilityStatusReachableViaWiFi)
//            {
//                [self showManualLogin];
//            }
//            else
//            {
//                
//            }
//        }];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - cell's button action
- (void)loginButtonDidTouch:(UIButton *)sender
{
    /// check network status then make login
    [[NetworkHandler sharedInstance] beginMonitorNetwork:^(AFNetworkReachabilityStatus result) {
        if (result == AFNetworkReachabilityStatusReachableViaWWAN)
        {
            [self autoLogin];
        }
        else if (result == AFNetworkReachabilityStatusReachableViaWiFi)
        {
            [self showManualLogin];
        }
        else
        {
            
        }
    }];
}

- (void)registerButtonDidtouch:(UIButton *)sender
{
//    RegisterViewController *registerVC = [[RegisterViewController alloc] initWithURL:[NSURL URLWithString:@"http://dautruongtrithuc.mobifone.com.vn/NewRegister.html"]];
//    [self presentViewController:registerVC animated:YES completion:^{
//        
//    }];
}

- (void)cancelButtonDidTouch:(UIButton *)sender
{
    
}

#pragma mark - action finish function
- (void)showManualLogin
{
    LoginViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self presentViewController:loginVC animated:YES completion:^{
        
    }];
    
}
- (void)autoLogin
{
//    if ([[TokenHelper sharedInstance] isLogedIn]) {
//        [self loginDidSuccess];
//    }
//    else
//    {
        [SVProgressHUD show];
        [[ServerDataServices sharedInstance] autoLogin:^(NSError *error, id response) {
            [SVProgressHUD dismiss];
            if (error)
            {
                [self loginDidSuccess];
            }
            else
            {
                NSDictionary *dict = response;
                NSString *token = dict[@"token"];
                [[TokenHelper sharedInstance] saveToken:token];
                [self loginDidSuccess];
            }
        }];

//    }
}
- (void)loginDidSuccess
{
    HomeViewController *homeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
    MenuViewController *menuVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
    UINavigationController *naviHome = [[UINavigationController alloc] initWithRootViewController:homeVC];
    naviHome.navigationBar.titleTextAttributes = @{
                                                   NSForegroundColorAttributeName: [UIColor whiteColor],
                                                   NSFontAttributeName: [UIFont systemFontOfSize:18]
                                                   };
    naviHome.navigationBar.tintColor = [UIColor whiteColor];
    naviHome.navigationBar.translucent = NO;
    naviHome.navigationBar.barTintColor = kMainBlueColor;
    naviHome.view.backgroundColor = kMainBlueColor;
    MFSideMenuContainerViewController *containerVC = [MFSideMenuContainerViewController containerWithCenterViewController:naviHome leftMenuViewController:menuVC rightMenuViewController:nil];
    containerVC.panMode = MFSideMenuPanModeNone;
    AppDelegate *appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    appdelegate.window.rootViewController = containerVC;
}

#pragma mark - collection view datasource
- (void)registerCustomCell
{
    [_collectionView registerNib:[UINib nibWithNibName:@"StartPageCell" bundle:nil] forCellWithReuseIdentifier:@"StartPageCell"];
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    StartPageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"StartPageCell" forIndexPath:indexPath];
    [cell.btnLogin addTarget:self action:@selector(loginButtonDidTouch:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnRegister addTarget:self action:@selector(registerButtonDidtouch:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnCancelService addTarget:self action:@selector(cancelButtonDidTouch:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 1;
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

#pragma mark - collection view delegate
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsZero;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [StartPageCell cellSize];
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    return CGSizeZero;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeZero;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}


@end
