//
//  MenuFunctionCell.m
//  dttt
//
//  Created by Nguyen Huy Quang on 1/6/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import "MenuFunctionCell.h"

@implementation MenuFunctionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)makeContentAtIndex:(NSIndexPath *)indexPath selectedIndex:(NSInteger)selectedIndex
{
    if (selectedIndex == indexPath.row) {
        _viewLineFull.hidden = NO;
        _viewLine.hidden = YES;
        self.backgroundColor = [UIColor colorWithRed:216.0/255.0 green:216.0/255.0 blue:216.0/255.0 alpha:0.4];
    }
    else
    {
        _viewLineFull.hidden = YES;
        _viewLine.hidden = NO;
        self.backgroundColor = [UIColor clearColor];
    }
    switch (indexPath.row) {
        case 0:
            break;
        case 1:
            _lblTitle.text = @"Trang chủ";
            break;
        case 2:
            _lblTitle.text = @"Giới thiệu";
            break;
        case 3:
            _lblTitle.text = @"Giải thưởng";
            break;
        case 4:
            _lblTitle.text = @"Kiểm tra gói cước";
            break;
        case 5:
            _lblTitle.text = @"Tra cứu điểm";
            break;
        case 6:
            _lblTitle.text = @"Danh sách trúng thưởng";
            break;
        case 7:
            _lblTitle.text = @"Đăng xuất";
            break;
        case 8:
            _lblTitle.text = @"Đăng Kí";
            break;
        default:
            break;
    }
}
@end
