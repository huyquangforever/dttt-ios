//
//  MenuViewController.m
//  dttt
//
//  Created by Nguyen Huy Quang on 1/6/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import "MenuViewController.h"
#import "MenuHeaderCell.h"
#import "MenuFunctionCell.h"
#import "HomeViewController.h"
#import "WebviewViewController.h"
#import "IntroViewController.h"
#import "RegisterViewController.h"
#import "ScoreViewController.h"
#import "CheckServicessViewController.h"
#import "ServerDataServices.h"
#import "NotificationView.h"
#import "LoginViewController.h"
@interface MenuViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
{
    NSInteger currentSelectedIndex;
}
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    currentSelectedIndex = 1;
    [self registerCustomCell];
    [self.menuContainerViewController setMenuStateDidChangeBlock:^(MFSideMenuState state) {
        if (state == MFSideMenuStateClosed)
        {
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
        }
        else
        {
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - show center view controller
- (void)showHomeViewController
{
    HomeViewController *homeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
    UINavigationController *naviHome = [[UINavigationController alloc] initWithRootViewController:homeVC];
    naviHome.navigationBar.titleTextAttributes = @{
                                                   NSForegroundColorAttributeName: [UIColor whiteColor],
                                                   NSFontAttributeName: [UIFont systemFontOfSize:18]
                                                   };
    naviHome.navigationBar.tintColor = [UIColor whiteColor];
    naviHome.navigationBar.translucent = NO;
    naviHome.navigationBar.barTintColor = kMainBlueColor;
    naviHome.view.backgroundColor = kMainBlueColor;
    self.menuContainerViewController.centerViewController = naviHome;
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    }];
}
- (void)showIntroViewController
{
    IntroViewController *introVC = [self.storyboard instantiateViewControllerWithIdentifier:@"IntroViewController"];
    UINavigationController *naviHome = [[UINavigationController alloc] initWithRootViewController:introVC];
    naviHome.navigationBar.titleTextAttributes = @{
                                                   NSForegroundColorAttributeName: [UIColor whiteColor],
                                                   NSFontAttributeName: [UIFont systemFontOfSize:18]
                                                   };
    naviHome.navigationBar.tintColor = [UIColor whiteColor];
    naviHome.navigationBar.translucent = NO;
    naviHome.navigationBar.barTintColor = kMainBlueColor;
    naviHome.view.backgroundColor = kMainBlueColor;
    introVC.introType = IntroTypeIntro;
    self.menuContainerViewController.centerViewController = naviHome;
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    }];
}
- (void)showRulesViewController
{
    IntroViewController *introVC = [self.storyboard instantiateViewControllerWithIdentifier:@"IntroViewController"];
    UINavigationController *naviHome = [[UINavigationController alloc] initWithRootViewController:introVC];
    naviHome.navigationBar.titleTextAttributes = @{
                                                   NSForegroundColorAttributeName: [UIColor whiteColor],
                                                   NSFontAttributeName: [UIFont systemFontOfSize:18]
                                                   };
    naviHome.navigationBar.tintColor = [UIColor whiteColor];
    naviHome.navigationBar.translucent = NO;
    naviHome.navigationBar.barTintColor = kMainBlueColor;
    naviHome.view.backgroundColor = kMainBlueColor;
    introVC.introType = IntroTypePrize;
    self.menuContainerViewController.centerViewController = naviHome;
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    }];
}
- (void)showWinnerList
{
//    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
//        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
//    }];
    SFSafariViewController *safariVC = [[SFSafariViewController alloc] initWithURL:[NSURL URLWithString:@"https://docs.google.com/spreadsheets/d/1fgMk_4u-eCznGRisU0A93E6azk4o6kfXEGbCgFxjds4/edit"]];
    [self presentViewController:safariVC animated:YES completion:^{
        
    }];
    
}

- (void)showTotalScore
{
    ScoreViewController *scoreVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ScoreViewController"];
    UINavigationController *naviHome = [[UINavigationController alloc] initWithRootViewController:scoreVC];
    naviHome.navigationBar.titleTextAttributes = @{
                                                   NSForegroundColorAttributeName: [UIColor whiteColor],
                                                   NSFontAttributeName: [UIFont systemFontOfSize:18]
                                                   };
    naviHome.navigationBar.tintColor = [UIColor whiteColor];
    naviHome.navigationBar.translucent = NO;
    naviHome.navigationBar.barTintColor = kMainBlueColor;
    naviHome.view.backgroundColor = kMainBlueColor;
    self.menuContainerViewController.centerViewController = naviHome;
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    }];

}
- (void)showCheckServices
{
    CheckServicessViewController *checkServicesVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CheckServicessViewController"];
    UINavigationController *naviHome = [[UINavigationController alloc] initWithRootViewController:checkServicesVC];
    naviHome.navigationBar.titleTextAttributes = @{
                                                   NSForegroundColorAttributeName: [UIColor whiteColor],
                                                   NSFontAttributeName: [UIFont systemFontOfSize:18]
                                                   };
    naviHome.navigationBar.tintColor = [UIColor whiteColor];
    naviHome.navigationBar.translucent = NO;
    naviHome.navigationBar.barTintColor = kMainBlueColor;
    naviHome.view.backgroundColor = kMainBlueColor;
    self.menuContainerViewController.centerViewController = naviHome;
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    }];
//    if ([[TokenHelper sharedInstance] isLogedIn]) {
//        [[ServerDataServices sharedInstance] checkServicesCompletion:^(NSError *error, id response) {
//            if (error) {
//                
//            }
//            else
//            {
//                NSString *main = response[@"main"];
//                [NotificationView showMessage:main];
//            }
//        }];
//    }
//    else
//    {
//        LoginViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
//        [self presentViewController:loginVC animated:YES completion:^{
//            
//        }];
//    }

    

}
- (void)signOut
{
    [UIAlertController showAlertWithTitle:@"Đăng xuất" message:@"Bạn chắc chắn muốn đăng xuất?" cancelTitle:@"Hủy" othersActionTitle:@[@"Đăng xuất"] cancelAction:^{
        
    } othersAction:^(NSString *action) {
        [[TokenHelper sharedInstance] signOut];
        [self showHomeViewController];
    } fromViewController:self];
}
- (void)registerNew
{
    RegisterViewController *registerVC = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterViewController"];
    UINavigationController *naviHome = [[UINavigationController alloc] initWithRootViewController:registerVC];
    naviHome.navigationBar.titleTextAttributes = @{
                                                   NSForegroundColorAttributeName: [UIColor whiteColor],
                                                   NSFontAttributeName: [UIFont systemFontOfSize:18]
                                                   };
    naviHome.navigationBar.tintColor = [UIColor whiteColor];
    naviHome.navigationBar.translucent = NO;
    naviHome.navigationBar.barTintColor = kMainBlueColor;
    naviHome.view.backgroundColor = kMainBlueColor;
    self.menuContainerViewController.centerViewController = naviHome;
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    }];


}
#pragma mark - collection view datasource
- (void)registerCustomCell
{
    [_collectionView registerNib:[UINib nibWithNibName:@"MenuHeaderCell" bundle:nil] forCellWithReuseIdentifier:@"MenuHeaderCell"];
    [_collectionView registerNib:[UINib nibWithNibName:@"MenuFunctionCell" bundle:nil] forCellWithReuseIdentifier:@"MenuFunctionCell"];
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        MenuHeaderCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MenuHeaderCell" forIndexPath:indexPath];
        
        return cell;
    }
    else
    {
        MenuFunctionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MenuFunctionCell" forIndexPath:indexPath];
        [cell makeContentAtIndex:indexPath selectedIndex:currentSelectedIndex];
        return cell;
    }
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 9;
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

#pragma mark - collection view delegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return;
    }
    
    switch (indexPath.row) {
        case 1:
            currentSelectedIndex = indexPath.row;
            [_collectionView reloadData];
            [self showHomeViewController];
            break;
        case 2:
            currentSelectedIndex = indexPath.row;
            [_collectionView reloadData];
            [self showIntroViewController];
            break;
        case 3:
            currentSelectedIndex = indexPath.row;
            [_collectionView reloadData];
            [self showRulesViewController];
            break;
        case 4:
            currentSelectedIndex = indexPath.row;
            [_collectionView reloadData];
            [self showCheckServices];
            break;
        case 5:
            currentSelectedIndex = indexPath.row;
            [_collectionView reloadData];
            [self showTotalScore];
            break;
        case 6:
            [self showWinnerList];
            break;
        case 7:
            [self signOut];
            break;
        case 8:
            currentSelectedIndex = indexPath.row;
            [_collectionView reloadData];
            [self registerNew];
            break;
        default:
            break;
    }
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsZero;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return CGSizeMake(collectionView.frame.size.width, 96);
    }
    else
    {
        return CGSizeMake(collectionView.frame.size.width, 50);
    }
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    return CGSizeZero;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeZero;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}


@end
