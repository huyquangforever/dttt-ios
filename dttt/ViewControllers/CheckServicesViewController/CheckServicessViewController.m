//
//  CheckServicessViewController.m
//  dttt
//
//  Created by Nguyen Huy Quang on 1/12/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import "CheckServicessViewController.h"
#import "ServerDataServices.h"
#import "NotificationView.h"
#import "ScoreCell.h"
#import "LoginViewController.h"
@interface CheckServicessViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
{
    BOOL isA, isD, isT, isL;
    NSString *mainString;
    
    NSInteger numberOfRow;
}
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@end

@implementation CheckServicessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initNavigationBarItem];
    [self registerCustomCell];
    if (![[TokenHelper sharedInstance] isLogedIn])
    {
        LoginViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        [self presentViewController:loginVC animated:YES completion:^{
            
        }];
    }
    
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if ([[TokenHelper sharedInstance] isLogedIn])
    {
        [self loadData];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - init navigation bar item
- (void)initNavigationBarItem
{
    self.navigationItem.title = @"Tra cứu gói cước";
    UIBarButtonItem *menuItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_menu"] style:UIBarButtonItemStyleDone target:self action:@selector(menuButtonDidTouch:)];
    self.navigationItem.leftBarButtonItem = menuItem;
}

#pragma mark - bar button item action
- (void)menuButtonDidTouch:(UIBarButtonItem *)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        
    }];
}
#pragma mark - load data
- (void)loadData
{
    
    [SVProgressHUD show];
    [[ServerDataServices sharedInstance] checkServicesCompletion:^(NSError *error, id response) {
        [SVProgressHUD dismiss];
        if (error)
        {
            if (error.code == 600)
            {
                NSString *message = response[@"message"];
                [NotificationView showMessage:message];
            }
            else
            {
                [NotificationView showMessage:error.localizedDescription];
            }
        }
        else
        {
            numberOfRow = 5;
            NSString *main = response[@"main"];
            NSArray *subs = [main componentsSeparatedByString:@","];
            if ([subs containsObject:@"A"])
            {
                isA = YES;
            }
            if ([subs containsObject:@"D"])
            {
                isD = YES;
            }
            if ([subs containsObject:@"T"])
            {
                isT = YES;
            }
            if ([subs containsObject:@"L"])
            {
                isL = YES;
            }
            if ([subs containsObject:@"NGAY"])
            {
                mainString = @"Gói ngày";
            }else
            if ([subs containsObject:@"TUAN"])
            {
                mainString = @"Gói tuần";
            }else
            if ([subs containsObject:@"THANG"])
            {
                mainString = @"Gói tháng";
            }else
            {
                mainString = @"Chưa đăng kí";
            }
            [_collectionView reloadData];
        }
    }];
}
#pragma mark - collection view datasource
- (void)registerCustomCell
{
    [_collectionView registerNib:[UINib nibWithNibName:@"ScoreCell" bundle:nil] forCellWithReuseIdentifier:@"ScoreCell"];
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ScoreCell *scoreCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ScoreCell" forIndexPath:indexPath];
    if (indexPath.row == 0) {
        [scoreCell setMainService:mainString];
    }
    else
    {
        switch (indexPath.row) {
            case 1:
                [scoreCell setPackageServices:isT type:indexPath.row];
                break;
            case 2:
                [scoreCell setPackageServices:isA type:indexPath.row];
                break;
            case 3:
                [scoreCell setPackageServices:isD type:indexPath.row];
                break;
            case 4:
                [scoreCell setPackageServices:isL type:indexPath.row];
                break;
            default:
                break;
        }
    }
    return scoreCell;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return numberOfRow;
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

#pragma mark - collection view delegate
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(20, 15, 20, 15);
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(collectionView.frame.size.width-40, 70);
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    return CGSizeZero;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeZero;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}

@end
