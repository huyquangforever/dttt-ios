//
//  RegisterViewController.m
//  dttt
//
//  Created by Nguyen Huy Quang on 1/9/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import "RegisterViewController.h"
#import "ListGameCell.h"
@interface RegisterViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation RegisterViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initNavigationBarItem];
    [self registerCustomCell];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
#pragma mark - init navigation bar item
- (void)initNavigationBarItem
{
    self.navigationItem.title = @"Đăng kí";
    UIBarButtonItem *menuItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_menu"] style:UIBarButtonItemStyleDone target:self action:@selector(menuButtonDidTouch:)];
    self.navigationItem.leftBarButtonItem = menuItem;
    
}

#pragma mark - bar button item action
- (void)menuButtonDidTouch:(UIBarButtonItem *)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        
    }];
}
#pragma mark - collection view datasource
- (void)registerCustomCell
{
    [_collectionView registerNib:[UINib nibWithNibName:@"ListGameCell" bundle:nil] forCellWithReuseIdentifier:@"ListGameCell"];
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ListGameCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ListGameCell" forIndexPath:indexPath];
    cell.lblJoin.text = @"Đăng kí";
    [cell makeContentAtIndexPath:indexPath];
    return cell;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 1;
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

#pragma mark - collection view delegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        SFSafariViewController *safariVC = [[SFSafariViewController alloc] initWithURL:[NSURL URLWithString:@"http://dautruongtrithuc.mobifone.com.vn/NewRegister.html"]];
        [self presentViewController:safariVC animated:YES completion:nil];

    }
    else
    {
        NSString *code = @"";
        switch (indexPath.row) {
            case 0:
                code = @"T";
                break;
            case 1:
                code = @"A";
                break;
            case 2:
                code = @"D";
                break;
            case 3:
                code = @"L";
                break;
            default:
                break;
        }
        NSString *url = [NSString stringWithFormat:@"http://dautruongtrithuc.mobifone.com.vn/WapGW/subscribe?pkgcode=%@&pkgid=1&price=2000&info=DTTT-2000&id=1&agentid=3",code];
        SFSafariViewController *safariVC = [[SFSafariViewController alloc] initWithURL:[NSURL URLWithString:url]];
        [self presentViewController:safariVC animated:YES completion:nil];

    }
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(15, 15, 15, 15);
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [ListGameCell cellSizeAtIndexPath:indexPath];
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    return CGSizeZero;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeZero;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 20;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}
@end
