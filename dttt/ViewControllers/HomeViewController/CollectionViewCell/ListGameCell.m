//
//  ListGameCell.m
//  dttt
//
//  Created by Nguyen Huy Quang on 1/6/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import "ListGameCell.h"

@implementation ListGameCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

+ (CGSize)cellSizeAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        CGFloat width = [UIScreen mainScreen].bounds.size.width - 30;
        CGFloat height = 180;
        return CGSizeMake(width, height);
    }
    else
    {
        CGFloat width = ([UIScreen mainScreen].bounds.size.width - 40)/2;
        CGFloat height = 180;
        return CGSizeMake(width, height);
    }
}

- (void)makeContentAtIndexPath:(NSIndexPath *)indexPath
{
    _imgvAvatar.layer.cornerRadius = _imgvAvatar.frame.size.width/2;
    switch (indexPath.row)
    {
        case 0:
            _lblTitle.text = @"ĐẤU TRƯỜNG CHÍNH";
            _imgvAvatar.image = [UIImage imageNamed:@"main_game"];
            _lblTitle.font = [UIFont fontWithName:_lblTitle.font.fontName size:20];
            _lblJoin.font = [UIFont fontWithName:_lblJoin.font.fontName size:14];
            _viewBackground.backgroundColor = [UIColor colorWithRed:142.0/255.0 green:68.0/255.0 blue:173.0/255.0 alpha:1.0];
            _viewBottom.backgroundColor = [UIColor colorWithRed:114.0/255.0 green:54.0/255.0 blue:138.0/255.0 alpha:1.0];
            _imgvAvatar.layer.borderColor = [UIColor colorWithRed:165.0/255.0 green:119.0/255.0 blue:185.0/255.0 alpha:1.0].CGColor;
            _imgvAvatar.layer.borderWidth = 4.0;
            _imgvAvatar.backgroundColor = [UIColor colorWithRed:142.0/255.0 green:68.0/255.0 blue:173.0/255.0 alpha:1.0];
            break;
        case 1:
            _lblTitle.text = @"THỂ THAO";
            _imgvAvatar.image = [UIImage imageNamed:@"sport"];
            _lblTitle.font = [UIFont fontWithName:_lblTitle.font.fontName size:14];
            _lblJoin.font = [UIFont fontWithName:_lblJoin.font.fontName size:10];
            _viewBackground.backgroundColor = [UIColor colorWithRed:52.0/255.0 green:73.0/255.0 blue:94.0/255.0 alpha:1.0];
            _viewBottom.backgroundColor = [UIColor colorWithRed:42.0/255.0 green:58.0/255.0 blue:75.0/255.0 alpha:1.0];
            _imgvAvatar.layer.borderColor = [UIColor colorWithRed:109.0/255.0 green:112.0/255.0 blue:136.0/255.0 alpha:1.0].CGColor;
            _imgvAvatar.layer.borderWidth = 3.0;
            _imgvAvatar.backgroundColor = [UIColor colorWithRed:52.0/255.0 green:73.0/255.0 blue:94.0/255.0 alpha:1.0];
            break;
        case 2:
            _lblTitle.text = @"ÂM NHẠC";
            _imgvAvatar.image = [UIImage imageNamed:@"music"];
            _lblTitle.font = [UIFont fontWithName:_lblTitle.font.fontName size:14];
            _lblJoin.font = [UIFont fontWithName:_lblJoin.font.fontName size:10];
            _viewBackground.backgroundColor = [UIColor colorWithRed:22.0/255.0 green:160.0/255.0 blue:133.0/255.0 alpha:1.0];
            _viewBottom.backgroundColor = [UIColor colorWithRed:18.0/255.0 green:128.0/255.0 blue:106.0/255.0 alpha:1.0];
            _imgvAvatar.layer.borderColor = [UIColor colorWithRed:127.0/255.0 green:192.0/255.0 blue:182.0/255.0 alpha:1.0].CGColor;
            _imgvAvatar.layer.borderWidth = 3.0;
            _imgvAvatar.backgroundColor = [UIColor colorWithRed:22.0/255.0 green:160.0/255.0 blue:133.0/255.0 alpha:1.0];
            break;
        case 3:
            _lblTitle.text = @"ĐỐ VUI";
            _imgvAvatar.image = [UIImage imageNamed:@"fun"];
            _lblTitle.font = [UIFont fontWithName:_lblTitle.font.fontName size:14];
            _lblJoin.font = [UIFont fontWithName:_lblJoin.font.fontName size:10];
            _viewBackground.backgroundColor = [UIColor colorWithRed:243.0/255.0 green:156.0/255.0 blue:18.0/255.0 alpha:1.0];
            _viewBottom.backgroundColor = [UIColor colorWithRed:194.0/255.0 green:124.0/255.0 blue:14.0/255.0 alpha:1.0];
            _imgvAvatar.layer.borderColor = [UIColor colorWithRed:229.0/255.0 green:174.0/255.0 blue:87.0/255.0 alpha:1.0].CGColor;
            _imgvAvatar.layer.borderWidth = 3.0;
            _imgvAvatar.backgroundColor = [UIColor colorWithRed:243.0/255.0 green:156.0/255.0 blue:18.0/255.0 alpha:1.0];
            break;
        case 4:
            _lblTitle.text = @"LỊCH SỬ & ĐỊA LÝ";
            _imgvAvatar.image = [UIImage imageNamed:@"history"];
            _lblTitle.font = [UIFont fontWithName:_lblTitle.font.fontName size:14];
            _lblJoin.font = [UIFont fontWithName:_lblJoin.font.fontName size:10];
            _viewBackground.backgroundColor = [UIColor colorWithRed:41.0/255.0 green:128.0/255.0 blue:185.0/255.0 alpha:1.0];
            _viewBottom.backgroundColor = [UIColor colorWithRed:33.0/255.0 green:102.0/255.0 blue:148.0/255.0 alpha:1.0];
            _imgvAvatar.layer.borderColor = [UIColor colorWithRed:173.0/255.0 green:201.0/255.0 blue:221.0/255.0 alpha:1.0].CGColor;
            _imgvAvatar.layer.borderWidth = 3.0;
            _imgvAvatar.backgroundColor = [UIColor colorWithRed:41.0/255.0 green:128.0/255.0 blue:185.0/255.0 alpha:1.0];
            break;
        default:
            break;
    }
}
@end
