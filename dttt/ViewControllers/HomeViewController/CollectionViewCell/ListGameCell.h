//
//  ListGameCell.h
//  dttt
//
//  Created by Nguyen Huy Quang on 1/6/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListGameCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *viewBackground;
@property (weak, nonatomic) IBOutlet UIView *viewBorderAvatar;
@property (weak, nonatomic) IBOutlet UIImageView *imgvAvatar;
@property (weak, nonatomic) IBOutlet UIView *viewBottom;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblJoin;
+ (CGSize)cellSizeAtIndexPath:(NSIndexPath *)indexPath;
- (void)makeContentAtIndexPath:(NSIndexPath *)indexPath;
@end
