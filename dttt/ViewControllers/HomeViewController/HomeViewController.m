//
//  HomeViewController.m
//  dttt
//
//  Created by Nguyen Huy Quang on 1/6/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import "HomeViewController.h"
#import "ListGameCell.h"
#import "GamePlayViewController.h"
#import "PackageGamePlayViewController.h"
#import "TokenHelper.h"
#import "LoginViewController.h"

@interface HomeViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
{
    UIBarButtonItem *loginItem;
}
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initNavigationBarItem];
    [self registerCustomCell];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [_collectionView reloadData];
    if ([[TokenHelper sharedInstance] isLogedIn])
    {
        self.navigationItem.rightBarButtonItem = nil;
    }
    else
    {
        self.navigationItem.rightBarButtonItem = loginItem;
    }
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - init navigation bar item
- (void)initNavigationBarItem
{
    self.navigationItem.title = @"Trang chủ";
    UIBarButtonItem *menuItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_menu"] style:UIBarButtonItemStyleDone target:self action:@selector(menuButtonDidTouch:)];
    self.navigationItem.leftBarButtonItem = menuItem;
    
    loginItem = [[UIBarButtonItem alloc] initWithTitle:@"Đăng nhập" style:UIBarButtonItemStylePlain target:self action:@selector(loginButtonDidTouch:)];
}

#pragma mark - bar button item action
- (void)menuButtonDidTouch:(UIBarButtonItem *)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        
    }];
}
- (void)loginButtonDidTouch:(id)sender
{
    LoginViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self presentViewController:loginVC animated:YES completion:^{
        
    }];
}
#pragma mark - collection view datasource
- (void)registerCustomCell
{
    [_collectionView registerNib:[UINib nibWithNibName:@"ListGameCell" bundle:nil] forCellWithReuseIdentifier:@"ListGameCell"];
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ListGameCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ListGameCell" forIndexPath:indexPath];
    [cell makeContentAtIndexPath:indexPath];
    return cell;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 5;
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

#pragma mark - collection view delegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (![[TokenHelper sharedInstance] isLogedIn]) {
        LoginViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        [self presentViewController:loginVC animated:YES completion:^{
            
        }];
        return;
    }
    if (indexPath.row == 0) {
        GamePlayViewController *gamePlayVC = [self.storyboard instantiateViewControllerWithIdentifier:@"GamePlayViewController"];
        gamePlayVC.gamePlayType = indexPath.row;
        [self.navigationController pushViewController:gamePlayVC animated:YES];
    }
    else
    {
        PackageGamePlayViewController *gamePlayVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PackageGamePlayViewController"];
        gamePlayVC.gamePlayType = indexPath.row;
        [self.navigationController pushViewController:gamePlayVC animated:YES];
    }
    
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(15, 15, 15, 15);
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [ListGameCell cellSizeAtIndexPath:indexPath];
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    return CGSizeZero;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeZero;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 20;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}
@end
