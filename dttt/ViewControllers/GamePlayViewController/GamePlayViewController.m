//
//  GamePlayViewController.m
//  dttt
//
//  Created by Nguyen Huy Quang on 1/6/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import "GamePlayViewController.h"
#import "QuestionCell.h"
#import "AnswerCell.h"
#import "ServerDataServices.h"
#import "NotificationView.h"
#import "MainQuestionEntity.h"
#import "LoginViewController.h"
#import "TokenHelper.h"
#import "RegisterViewController.h"
#import <SafariServices/SafariServices.h>
@interface GamePlayViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
{
    NSInteger numberOfAnswer;
    NSInteger numberOfQuestion;
    NSInteger timeToCountDown;
    NSString *question;
    MainQuestionEntity *mainQuestion;
    
    NSTimer *timerCountDown;
    BOOL isCountingDown;
    BOOL isUsingLuckyStar;
    NSInteger currentAnswer;
    BOOL hasSendAnswer;
    BOOL stageEnded;
}
@property (weak, nonatomic) IBOutlet UILabel *countDownLabel;
@property (weak, nonatomic) IBOutlet UILabel *questionNumerLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIButton *btnLuckyStar;

@end

@implementation GamePlayViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
    [self registerCustomCell];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;

    if ([[TokenHelper sharedInstance] isLogedIn]) {
        [self loadQuestion];
    }
    
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (isCountingDown)
    {
        NSInteger endTime = [MainQuestionEntity currentQuestion].EndTime;
        NSInteger currentTimeInterval = [[NSDate date] timeIntervalSince1970];
        timeToCountDown = endTime/1000 - currentTimeInterval;
    }

}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
}
- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [timerCountDown invalidate];
    timerCountDown = nil;
    [[CoundownHelper sharedInstance] countdownDidUpdated:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)back:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)selectLuckyStar:(UIButton *)sender {
    isUsingLuckyStar = !isUsingLuckyStar;
    sender.selected = isUsingLuckyStar;
}
#pragma mark - load question
- (void)loadQuestion
{
    [SVProgressHUD show];
    NSString *code = @"CH";
    self.countDownLabel.hidden = YES;
    [[ServerDataServices sharedInstance] getQuestion:code completionBlock:^(NSError *error, id response) {
        [SVProgressHUD dismiss];
        if (error)
        {
            if (error.code == 401)
            {
                [UIAlertController showAlertWithTitle:@"Đăng nhập đã hết hạn" message:@"Bạn cần phải đăng nhập lại để tiếp tục." cancelTitle:@"Đăng nhập" othersActionTitle:nil cancelAction:^{
                    [[TokenHelper sharedInstance] signOut];
                    LoginViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                    [self presentViewController:loginVC animated:YES completion:^{
                        
                    }];
                } othersAction:nil];
            }
            else if (error.code == 404 || error.code == 500)
            {
                [UIAlertController showAlertWithTitle:@"Đấu trường tri thức" message:error.localizedDescription cancelTitle:@"Đóng" othersActionTitle:nil cancelAction:^{
                    [self.navigationController popViewControllerAnimated:YES];
                } othersAction:nil];
            }else
            {
                NSString *message = response[@"message"];
                NSInteger code = [response[@"code"] integerValue];
                if (code == 104)
                {
//                    [UIAlertController showAlertWithTitle:@"Chưa đăng kí đấu trường" message:message cancelTitle:@"Đóng" othersActionTitle:@[@"Đăng kí"] cancelAction:^{
                    
//                    } othersAction:^(NSString *action) {
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        NSString *url = @"http://dautruongtrithuc.mobifone.com.vn/NewRegister.html";
                        SFSafariViewController *safariVC = [[SFSafariViewController alloc] initWithURL:[NSURL URLWithString:url]];
                        [self presentViewController:safariVC animated:YES completion:nil];
                    });
                    //                    RegisterViewController *registerVC = [[RegisterViewController alloc] initWithURL:[NSURL URLWithString:@"http://dautruongtrithuc.mobifone.com.vn/NewRegister.html"]];
//                    registerVC.needUpdateStatusBar = YES;
//                    [self presentViewController:registerVC animated:YES completion:^{
//                            
//                    }];
//                    } fromViewController:self];
                    return;
                }
                if (code == 106 || code == 100 || code == 101 || code == 103 || code == 105 || code == 107 || code == 108 || code == 99) {
                    stageEnded = YES;
                    isCountingDown = NO;
                }
                
                NSLog(@"%@",response);
                NSInteger endTime = response[@"endtime"]?[response[@"endtime"] integerValue]:-1;
                if (endTime > 0)
                {
                    [MainQuestionEntity currentQuestion].question = message;
                    [MainQuestionEntity currentQuestion].EndTime = endTime;
                    question = message;
                    numberOfQuestion = 1;
                    numberOfAnswer = 0;
                    NSInteger currentTimeInterval = [[NSDate date] timeIntervalSince1970];
                    timeToCountDown = endTime/1000 - currentTimeInterval;
                
                    [[NotificationHelper sharedInstance] makeQuestionNotifyWithMessage:@"Hãy trả lời câu hỏi tiếp theo!" fireDate:[NSDate dateWithTimeIntervalSince1970:endTime/1000]];
                    self.countDownLabel.hidden = NO;
                    [_collectionView reloadData];
                    [self countDownTime];
                }
                else
                {
                    [NotificationView showMessage:message];
                }
                
            }
            
        }
        else
        {
            self.countDownLabel.hidden = NO;
            NSDictionary *dict = response;
            [self setQuestionData:dict];
        }
    }];
}
- (void)setQuestionData:(NSDictionary *)data
{
    if (_gamePlayType == GamePlayTypeMain)
    {
        [[MainQuestionEntity currentQuestion] parseObject:data];
        mainQuestion = [[MainQuestionEntity alloc] init];
        [mainQuestion parseObject:data];
        question = data[@"question"];
        if (mainQuestion.QuestionNum >= 5)
        {
            _btnLuckyStar.hidden = NO;
            _btnLuckyStar.selected = NO;
            isUsingLuckyStar = NO;
        }
        numberOfQuestion = 1;
        numberOfAnswer = mainQuestion.AnswerNum;
        NSInteger endTime = mainQuestion.EndTime;
        NSInteger currentTimeInterval = [[NSDate date] timeIntervalSince1970];
        timeToCountDown = endTime/1000 - currentTimeInterval;
        [self countDownTime];
        NSInteger questionNum = [data[@"QuestionNum"] integerValue];
        NSString *questionNumString = [NSString stringWithFormat:@"Câu %d",questionNum];
        self.questionNumerLabel.text = questionNumString;
        [[NotificationHelper sharedInstance] makeQuestionNotifyWithMessage:@"Hãy trả lời câu hỏi tiếp theo!" fireDate:[NSDate dateWithTimeIntervalSince1970:endTime/1000]];
        
        [self.collectionView reloadData];
        if (mainQuestion.QuestionNum != 1)
        {
            NSString *message = [NSString stringWithFormat:@"Hoàn toàn chính xác! Bạn đã loại được %d người chơi, số điểm bạn giành được là %d. Hãy chinh phục câu hỏi tiếp theo",mainQuestion.Player_Lost,mainQuestion.Score_Play_Win];
            [NotificationView showMessage:message];
        }
    }
    
}
- (void)loadResult
{
//    [[ServerDataServices sharedInstance] getAnswerResult:^(NSError *error, id response) {
//        if (error)
//        {
//            
//        }
//        else
//        {
//            NSDictionary *dict = response;
//            [NotificationView showMessage:@""];
//        }
//    }];
}
- (void)sendAnswer
{
    [SVProgressHUD show];
    
    NSString *answer = [NSString stringWithFormat:@"%d",currentAnswer] ;
    if (isUsingLuckyStar)
    {
        answer = [NSString stringWithFormat:@"S%d",currentAnswer];
    }
    isUsingLuckyStar = NO;
    [_btnLuckyStar setHidden:YES];
    [_btnLuckyStar setSelected:NO];
    [[ServerDataServices sharedInstance] postAnswer:answer questionType:@"" completionBlock:^(NSError *error, id response) {
        
        if (error)
        {
            [SVProgressHUD dismiss];
            if (error.code == 401)
            {
                [UIAlertController showAlertWithTitle:@"Đăng nhập đã hết hạn" message:@"Bạn cần phải đăng nhập lại để tiếp tục." cancelTitle:@"Đăng nhập" othersActionTitle:nil cancelAction:^{
                    [[TokenHelper sharedInstance] signOut];
                    LoginViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                    [self presentViewController:loginVC animated:YES completion:^{
                        
                    }];
                } othersAction:nil];
            }
            else if (error.code == 404 || error.code == 500)
            {
                
            }
            else
            {
//                NSString *message = response[@"message"];
//                message = [message stringByAppendingString:@"\n"];
//                [NotificationView showMessage:message];
//                [self loadQuestion];
                question = [MainQuestionEntity currentQuestion].question = @"Hệ thống đã tiếp nhận câu trả lời của bạn. Vui lòng chờ câu hỏi tiếp theo.";
                numberOfAnswer = 0;
                numberOfQuestion = 1;
                [self.collectionView reloadData];
            }
        }
        else
        {
            NSDictionary *dict = response;
            [NotificationView showMessage:@"Hệ thống đã tiếp nhận câu trả lời của bạn. Vui lòng chờ câu hỏi tiếp theo."];
            [self loadQuestion];
        }
    }];
}
#pragma mark - handle error

#pragma mark - countdown the time
- (void)countDownTime
{
    if (!isCountingDown)
    {
        isCountingDown = YES;
        __weak typeof(self) weakSelf = self;
        [[CoundownHelper sharedInstance] countdownDidUpdated:^{
            __strong typeof(weakSelf) strongSelf = weakSelf;
            if (!stageEnded)
            {
                [strongSelf countDownTime];
            }
            
        }];
    }
    else
    {
        if (timeToCountDown <= 0)
        {
            [timerCountDown invalidate];
            timerCountDown = nil;
            isCountingDown = NO;
//            if ([MainQuestionEntity currentQuestion].QuestionNum < 10)
//            {
            [self loadQuestion];
//            }
//            else
//            {
//                stageEnded = YES;
//            }
            
        }
        else
        {
            NSInteger hour = timeToCountDown/3600;
            NSInteger subHour = timeToCountDown%3600;
            NSInteger minute = subHour/60;
            NSInteger second = subHour%60;
            NSString *hourString = [NSString stringWithFormat:@"%02d:%02d:%02d", hour, minute, second];
            _countDownLabel.text = hourString;
            NSInteger endTime = [MainQuestionEntity currentQuestion].EndTime;
            NSInteger currentTimeInterval = [[NSDate date] timeIntervalSince1970];
            timeToCountDown = endTime/1000 - currentTimeInterval;
        }
    }
}
#pragma mark - collection view datasource
- (void)registerCustomCell
{
    [_collectionView registerNib:[UINib nibWithNibName:@"AnswerCell" bundle:nil] forCellWithReuseIdentifier:@"AnswerCell"];
    [_collectionView registerNib:[UINib nibWithNibName:@"QuestionCell" bundle:nil] forCellWithReuseIdentifier:@"QuestionCell"];
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        QuestionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"QuestionCell" forIndexPath:indexPath];
        question = [MainQuestionEntity currentQuestion].question;
        [cell setQuestion:question];
        return cell;
    }
    else
    {
        AnswerCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"AnswerCell" forIndexPath:indexPath];
        cell.lblAnswer.text = [NSString stringWithFormat:@"Đáp án %d", indexPath.row + 1];
        return cell;
    }
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (section == 0)
    {
        return numberOfQuestion;
    }
    else
    {
        return numberOfAnswer;
    }
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 2;
}

#pragma mark - collection view delegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1)
    {
        currentAnswer = indexPath.row +1;
        [UIAlertController showAlertWithTitle:@"" message:[NSString stringWithFormat:@"Bạn có chắc chắn câu trả lời của bạn là đáp án %d?",currentAnswer] cancelTitle:@"Chọn lại" othersActionTitle:@[@"Đúng"] cancelAction:^{
            
        } othersAction:^(NSString *action) {
            [self sendAnswer];
        } fromViewController:self];
        
    }
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if (section == 0)
    {
        return UIEdgeInsetsMake(0, 15, 60, 15);
    }
    else
    {
        return UIEdgeInsetsMake(0, 15, 15, 15);
    }
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return [QuestionCell cellSizeWithQuestion:question];
    }
    else
    {
        return [AnswerCell cellSize];
    }
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    return CGSizeZero;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeZero;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 15;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 15;
}

@end
