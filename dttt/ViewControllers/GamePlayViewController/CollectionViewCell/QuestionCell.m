//
//  QuestionCell.m
//  dttt
//
//  Created by Nguyen Huy Quang on 1/6/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import "QuestionCell.h"
#import "NSString+Size.h"
@implementation QuestionCell
{
    NSString *currentQuestion;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
+(CGSize)cellSizeWithQuestion:(NSString *)question
{
    NSInteger maxWidth = [UIScreen mainScreen].bounds.size.width - 60;
    CGSize questionSize = [question usedSizeForMaxWidth:maxWidth withFont:[UIFont systemFontOfSize:17]];
    NSInteger height = questionSize.height + 16;
    return CGSizeMake([UIScreen mainScreen].bounds.size.width-30, height);
}
-(void)setQuestion:(NSString *)question
{
    currentQuestion = question;
    _lblQuestion.text = question;
}
@end
