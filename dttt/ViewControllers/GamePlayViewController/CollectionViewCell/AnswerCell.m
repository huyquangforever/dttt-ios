//
//  AnswerCell.m
//  dttt
//
//  Created by Nguyen Huy Quang on 1/6/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import "AnswerCell.h"

@implementation AnswerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
+(CGSize)cellSize
{
    return CGSizeMake([UIScreen mainScreen].bounds.size.width-30, 50);
}
@end
