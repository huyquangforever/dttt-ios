//
//  QuestionCell.h
//  dttt
//
//  Created by Nguyen Huy Quang on 1/6/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblQuestion;
- (void)setQuestion:(NSString *)question;
+ (CGSize)cellSizeWithQuestion:(NSString *)question;
@end
