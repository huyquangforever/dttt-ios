//
//  ScoreCell.h
//  dttt
//
//  Created by Nguyen Huy Quang on 1/12/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ScoreEntity.h"
@interface ScoreCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgvIcon;
@property (weak, nonatomic) IBOutlet UIView *viewBackground;

- (void)setScore:(ScoreEntity *)score type:(NSInteger)type;
- (void)setMainService:(NSString *)text;
- (void)setPackageServices:(BOOL)isRegister type:(NSInteger)type;
@end
