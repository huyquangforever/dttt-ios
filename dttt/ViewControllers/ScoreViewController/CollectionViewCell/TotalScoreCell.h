//
//  TotalScoreCell.h
//  dttt
//
//  Created by Nguyen Huy Quang on 1/12/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ScoreEntity.h"
@interface TotalScoreCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTotalScore;
- (void)setScore:(ScoreEntity *)entity;
@end
