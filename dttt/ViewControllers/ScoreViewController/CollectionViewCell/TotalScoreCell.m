//
//  TotalScoreCell.m
//  dttt
//
//  Created by Nguyen Huy Quang on 1/12/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import "TotalScoreCell.h"

@implementation TotalScoreCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)setScore:(ScoreEntity *)entity
{
    NSInteger totalScore = 0;
    if (![entity.main isEqualToString:@"Chưa tham gia"]) {
        totalScore += [entity.main integerValue];
    }
    if (![entity.sport isEqualToString:@"Chưa tham gia"]) {
        totalScore += [entity.sport integerValue];
    }
    if (![entity.music isEqualToString:@"Chưa tham gia"]) {
        totalScore += [entity.music integerValue];
    }
    if (![entity.fun isEqualToString:@"Chưa tham gia"]) {
        totalScore += [entity.fun integerValue];
    }
    if (![entity.history isEqualToString:@"Chưa tham gia"]) {
        totalScore += [entity.history integerValue];
    }
    
    _lblTotalScore.text = [NSString stringWithFormat:@"Tổng điểm: %d", totalScore];
}
@end
