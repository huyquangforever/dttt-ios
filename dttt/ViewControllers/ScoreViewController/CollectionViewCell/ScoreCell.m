//
//  ScoreCell.m
//  dttt
//
//  Created by Nguyen Huy Quang on 1/12/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import "ScoreCell.h"

@implementation ScoreCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
}
-(void)setScore:(ScoreEntity *)score type:(NSInteger)type
{
    
    switch (type) {
        case 1:
            _imgvIcon.image = [UIImage imageNamed:@"main_game"];
            _lblTitle.text = [NSString stringWithFormat:@"Đấu trường chính: %@",score.main];
            _imgvIcon.backgroundColor = _viewBackground.backgroundColor = [UIColor colorWithRed:142.0/255.0 green:68.0/255.0 blue:173.0/255.0 alpha:1.0];
            
            break;
        case 2:
            _imgvIcon.image = [UIImage imageNamed:@"sport"];
            _lblTitle.text = [NSString stringWithFormat:@"Gói thể thao: %@",score.sport];
            _imgvIcon.backgroundColor = _viewBackground.backgroundColor = [UIColor colorWithRed:52.0/255.0 green:73.0/255.0 blue:94.0/255.0 alpha:1.0];
            break;
        case 3:
            _imgvIcon.image = [UIImage imageNamed:@"music"];
            _lblTitle.text = [NSString stringWithFormat:@"Gói âm nhạc: %@",score.music];
            _imgvIcon.backgroundColor = _viewBackground.backgroundColor = [UIColor colorWithRed:22.0/255.0 green:160.0/255.0 blue:133.0/255.0 alpha:1.0];
            break;
        case 4:
            _imgvIcon.image = [UIImage imageNamed:@"fun"];
            _lblTitle.text = [NSString stringWithFormat:@"Gói đố vui: %@",score.fun];
            _imgvIcon.backgroundColor = _viewBackground.backgroundColor = [UIColor colorWithRed:243.0/255.0 green:156.0/255.0 blue:18.0/255.0 alpha:1.0];
            break;
        case 5:
            _imgvIcon.image = [UIImage imageNamed:@"history"];
            _lblTitle.text = [NSString stringWithFormat:@"Gói lịch sử - địa lí: %@",score.history];
            _imgvIcon.backgroundColor = _viewBackground.backgroundColor = [UIColor colorWithRed:41.0/255.0 green:128.0/255.0 blue:185.0/255.0 alpha:1.0];
            break;
        default:
            break;
    }
}
- (void)setMainService:(NSString *)text
{
    _imgvIcon.image = [UIImage imageNamed:@"main_game"];
    _lblTitle.text = [NSString stringWithFormat:@"Đấu trường chính: %@",text];
    _imgvIcon.backgroundColor = _viewBackground.backgroundColor = [UIColor colorWithRed:142.0/255.0 green:68.0/255.0 blue:173.0/255.0 alpha:1.0];
}
- (void)setPackageServices:(BOOL)isRegister type:(NSInteger)type
{
    NSString *text = isRegister?@"Gói ngày":@"Đã Hủy";
    switch (type) {
        case 1:
            _imgvIcon.image = [UIImage imageNamed:@"sport"];
            _lblTitle.text = [NSString stringWithFormat:@"Gói thể thao: %@",text];
            _imgvIcon.backgroundColor = _viewBackground.backgroundColor = [UIColor colorWithRed:52.0/255.0 green:73.0/255.0 blue:94.0/255.0 alpha:1.0];
            break;
        case 2:
            _imgvIcon.image = [UIImage imageNamed:@"music"];
            _lblTitle.text = [NSString stringWithFormat:@"Gói âm nhạc: %@",text];
            _imgvIcon.backgroundColor = _viewBackground.backgroundColor = [UIColor colorWithRed:22.0/255.0 green:160.0/255.0 blue:133.0/255.0 alpha:1.0];
            break;
        case 3:
            _imgvIcon.image = [UIImage imageNamed:@"fun"];
            _lblTitle.text = [NSString stringWithFormat:@"Gói đố vui: %@",text];
            _imgvIcon.backgroundColor = _viewBackground.backgroundColor = [UIColor colorWithRed:243.0/255.0 green:156.0/255.0 blue:18.0/255.0 alpha:1.0];
            break;
        case 4:
            _imgvIcon.image = [UIImage imageNamed:@"history"];
            _lblTitle.text = [NSString stringWithFormat:@"Gói lịch sử - địa lí: %@",text];
            _imgvIcon.backgroundColor = _viewBackground.backgroundColor = [UIColor colorWithRed:41.0/255.0 green:128.0/255.0 blue:185.0/255.0 alpha:1.0];
            break;
        default:
            break;
    }

}

@end
