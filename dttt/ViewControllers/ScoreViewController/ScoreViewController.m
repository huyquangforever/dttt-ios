//
//  ScoreViewController.m
//  dttt
//
//  Created by Nguyen Huy Quang on 1/12/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import "ScoreViewController.h"
#import "ServerDataServices.h"
#import "ScoreCell.h"
#import "ScoreEntity.h"
#import "TokenHelper.h"
#import "LoginViewController.h"
#import "TotalScoreCell.h"
@interface ScoreViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) ScoreEntity *scoreEntity;
@end

@implementation ScoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initNavigationBarItem];
    [self registerCustomCell];
    if (![[TokenHelper sharedInstance] isLogedIn]) {
        LoginViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        [self presentViewController:loginVC animated:YES completion:^{
            
        }];
    }
    
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    _collectionView.hidden = YES;
    if ([[TokenHelper sharedInstance] isLogedIn])
    {
        [self loadScore];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - init navigation bar item
- (void)initNavigationBarItem
{
    self.navigationItem.title = @"Tra cứu điểm";
    UIBarButtonItem *menuItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_menu"] style:UIBarButtonItemStyleDone target:self action:@selector(menuButtonDidTouch:)];
    self.navigationItem.leftBarButtonItem = menuItem;
}

#pragma mark - bar button item action
- (void)menuButtonDidTouch:(UIBarButtonItem *)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        
    }];
}
#pragma mark - load score
- (void)loadScore
{
    [SVProgressHUD show];
    [[ServerDataServices sharedInstance] getScoreCompletion:^(NSError *error, id response) {
        [SVProgressHUD dismiss];
        if (error)
        {
            if (error.code == 401) {
                [UIAlertController showAlertWithTitle:@"Đăng nhập đã hết hạn" message:@"Bạn cần phải đăng nhập lại để tiếp tục." cancelTitle:@"Đăng nhập" othersActionTitle:nil cancelAction:^{
                    [[TokenHelper sharedInstance] signOut];
                    LoginViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                    [self presentViewController:loginVC animated:YES completion:^{
                        
                    }];
                } othersAction:nil];
            }
        }
        else
        {
            _collectionView.hidden = NO;
            _scoreEntity = [[ScoreEntity alloc] initWithDictionary:response];
            [_collectionView reloadData];
        }
    }];
}

#pragma mark - collection view datasource
- (void)registerCustomCell
{
    [_collectionView registerNib:[UINib nibWithNibName:@"ScoreCell" bundle:nil] forCellWithReuseIdentifier:@"ScoreCell"];
    [_collectionView registerNib:[UINib nibWithNibName:@"TotalScoreCell" bundle:nil] forCellWithReuseIdentifier:@"TotalScoreCell"];
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        TotalScoreCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TotalScoreCell" forIndexPath:indexPath];
        [cell setScore:_scoreEntity];
        return cell;
    }
    ScoreCell *scoreCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ScoreCell" forIndexPath:indexPath];
    [scoreCell setScore:_scoreEntity type:indexPath.row];
    return scoreCell;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 6;
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

#pragma mark - collection view delegate
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(20, 15, 20, 15);
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(collectionView.frame.size.width-40, 70);
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    return CGSizeZero;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeZero;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}

@end
