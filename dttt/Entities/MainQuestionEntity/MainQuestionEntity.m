//
//  MainQuestionEntity.m
//  dttt
//
//  Created by Nguyen Huy Quang on 1/11/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import "MainQuestionEntity.h"

@implementation MainQuestionEntity
+ (instancetype)currentQuestion
{
    static MainQuestionEntity *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[MainQuestionEntity alloc] init];
    });
    return instance;
}
-(void)parseObject:(NSDictionary *)dict
{
    self.EndTime = dict[@"EndTime"]?[dict[@"EndTime"] integerValue]:0;
    self.Player_Lost = dict[@"Player_Lost"]?[dict[@"Player_Lost"] integerValue]:0;
    self.QuestionID = dict[@"QuestionID"]?[dict[@"QuestionID"] integerValue]:0;
    self.QuestionNum = dict[@"QuestionNum"]?[dict[@"QuestionNum"] integerValue]:0;
    self.Score_Play_Win = dict[@"Score_Play_Win"]?[dict[@"Score_Play_Win"] integerValue]:0;
    self.AnswerNum = dict[@"AnswerNum"]?[dict[@"AnswerNum"] integerValue]:0;
    self.question = dict[@"question"]?:@"";
}
@end
