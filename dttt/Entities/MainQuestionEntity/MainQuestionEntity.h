//
//  MainQuestionEntity.h
//  dttt
//
//  Created by Nguyen Huy Quang on 1/11/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MainQuestionEntity : NSObject
@property (assign, nonatomic) NSInteger EndTime;
@property (assign, nonatomic) NSInteger Player_Lost;
@property (assign, nonatomic) NSInteger QuestionID;
@property (assign, nonatomic) NSInteger QuestionNum;
@property (assign, nonatomic) NSInteger Score_Play_Win;
@property (strong, nonatomic) NSString *question;
@property (assign, nonatomic) NSInteger AnswerNum;
+ (instancetype)currentQuestion;
- (void)parseObject:(NSDictionary *)dict;
@end
