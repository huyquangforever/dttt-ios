//
//  ScoreEntity.m
//  dttt
//
//  Created by Nguyen Huy Quang on 1/12/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import "ScoreEntity.h"

@implementation ScoreEntity
- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if (self)
    {
        if (dict && dict[@"main"] != nil && ![dict[@"main"] isEqual:[NSNull null]] && ![dict[@"main"] isEqual:@"NULL"]) {
            _main = dict[@"main"];
        }
        else
        {
            _main = @"Chưa tham gia";
        }
        if (dict && dict[@"tt"] != nil && ![dict[@"tt"] isEqual:[NSNull null]] && ![dict[@"tt"] isEqual:@"NULL"]) {
            _sport = dict[@"tt"];
        }
        else
        {
            _sport = @"Chưa tham gia";
        }
        if (dict && dict[@"an"] != nil && ![dict[@"an"] isEqual:[NSNull null]] && ![dict[@"an"] isEqual:@"NULL"]) {
            _music = dict[@"an"];
        }
        else
        {
            _music = @"Chưa tham gia";
        }
        if (dict && dict[@"dv"] != nil && ![dict[@"dv"] isEqual:[NSNull null]] && ![dict[@"dv"] isEqual:@"NULL"]) {
            _fun = dict[@"dv"];
        }
        else
        {
            _fun = @"Chưa tham gia";
        }
        if (dict && dict[@"ls"] != nil && ![dict[@"ls"] isEqual:[NSNull null]] && ![dict[@"ls"] isEqual:@"NULL"]) {
            _history = dict[@"ls"];
        }
        else
        {
            _history = @"Chưa tham gia";
        }
    }
    return self;
}
@end
