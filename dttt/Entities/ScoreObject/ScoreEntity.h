//
//  ScoreEntity.h
//  dttt
//
//  Created by Nguyen Huy Quang on 1/12/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ScoreEntity : NSObject
@property (strong, nonatomic) NSString *main;
@property (strong, nonatomic) NSString *sport;
@property (strong, nonatomic) NSString *music;
@property (strong, nonatomic) NSString *fun;
@property (strong, nonatomic) NSString *history;

- (instancetype) initWithDictionary:(NSDictionary *)dict;
@end
