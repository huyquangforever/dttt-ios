//
//  PackageQuestionEntity.m
//  dttt
//
//  Created by Nguyen Huy Quang on 1/11/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import "PackageQuestionEntity.h"

@implementation PackageQuestionEntity
- (void)parseObject:(NSDictionary *)dict
{
    self.QuestionID = dict[@"QuestionID"]?[dict[@"QuestionID"] integerValue]:0;
    self.QuestionNum = dict[@"QuestionNum"]?[dict[@"QuestionNum"] integerValue]:0;
    self.question = dict[@"question"]?:@"";
    self.numAnswer = dict[@"AnswerNum"]?[dict[@"AnswerNum"] integerValue]:2;
}
@end
