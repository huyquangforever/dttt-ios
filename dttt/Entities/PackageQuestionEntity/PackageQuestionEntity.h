//
//  PackageQuestionEntity.h
//  dttt
//
//  Created by Nguyen Huy Quang on 1/11/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PackageQuestionEntity : NSObject
@property (assign, nonatomic) NSInteger QuestionID;
@property (assign, nonatomic) NSInteger QuestionNum;
@property (strong, nonatomic) NSString *question;
@property (assign, nonatomic) NSInteger numAnswer;
- (void)parseObject:(NSDictionary *)dict;
@end
