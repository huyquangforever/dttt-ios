//
//  AppDelegate.m
//  dttt
//
//  Created by Nguyen Huy Quang on 1/5/17.
//  Copyright © 2017 Nguyen Huy Quang. All rights reserved.
//

#import "AppDelegate.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [self registerNotificationSettings];
    [Fabric with:@[[Crashlytics class]]];
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    [[CoundownHelper sharedInstance] stopTimer];
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [[CoundownHelper sharedInstance] stopTimer];
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    [[CoundownHelper sharedInstance] startTimer];
    
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [[CoundownHelper sharedInstance] startTimer];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [[CoundownHelper sharedInstance] stopTimer];
}
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    [self makeLocalNotification];
}
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    [NotificationView showMessage:notification.alertBody];
}
#pragma mark - register notification settings
- (void)registerNotificationSettings
{
    [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
}

#pragma mark - make notification
- (void)makeLocalNotification
{
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    localNotification.alertBody = @"Sắp đến giờ tham gia Đấu Trường.";
    localNotification.timeZone = [NSTimeZone systemTimeZone];
    localNotification.repeatCalendar = [NSCalendar currentCalendar];
    localNotification.repeatInterval = NSCalendarUnitDay;
    NSDate *today = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [calendar componentsInTimeZone:[NSTimeZone systemTimeZone] fromDate:today];
    dateComponents.hour = 8;
    dateComponents.minute = 55;
    dateComponents.second = 0;
    NSDate *fireDate = [calendar dateFromComponents:dateComponents];
    localNotification.fireDate = fireDate;
    localNotification.userInfo = @{@"key":@"kMainNotifyKey"};
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}
@end
